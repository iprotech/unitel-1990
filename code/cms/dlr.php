<?php
$conn = new sbMysqlPDO("localhost","root","isun2011","voice_service");
$sql = "UPDATE dlr SET status='999' WHERE id=2 ";
$conn->doUpdate($sql);
$list = "\nAAA\n";
foreach($_REQUEST as $key=>$value){
    $list .= $key."|".$value."\n";
}
file_put_contents("/var/www/html/abc.txt",$list,FILE_APPEND);
echo urlencode("http://127.0.0.1/dlr.php?type=%d&state=%d&msisdn=%p&smsid=%I&coding=%c&time=%t");

class sbMysqlPDO{

   private $con,$error;

   function __construct($hostname,$username,$password,$dbname){
       $this->error = array();
       try{
           $this->con = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$password");
           $this->con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
       }catch(PDOException $e){
           $this->error[] = $e->getMessage();
           //echo $e->getMessage();
       }
   }

   public function getConnType(){
       return "mysql";
   }

   public function doSelect($sql){
       try{
           $stmt = $this->con->prepare($sql);
           $stmt->execute();
           return $stmt->fetchAll();
       }catch(PDOException $e){
          $this->error[] = array("query"=>$sql,"error"=>$e->getMessage());
          //echo $e->getMessage();
          return false;
       }
   }

   public function doSelectOne($sql){
       try{
           $stmt = $this->con->prepare($sql);
           $stmt->execute();
           $data = $stmt->fetchAll();
           if($data) return $data[0];
       }catch(PDOException $e){
           $this->error[] = array("query"=>$sql,"error"=>$e->getMessage());
           //echo $e->getMessage();
           return false;
       }
   }

   public function getLastInsertId(){
       return $this->con->lastInsertId();
   }

   public function doUpdate($sql){
       try{
           $this->con->exec($sql);
           if($this->getLastInsertId()) return $this->getLastInsertId();
           else return true;
       }catch(PDOException $e){
           $this->error[] = array("query"=>$sql,"error"=>$e->getMessage());
           //echo $e->getMessage();
           return false;
       }
   }

   public function getConnection(){
       return $this->con;
   }

   public function getError(){
       return $this->error;
   }

   public function closeConnection(){
       $this->con = NULL;
   }
}
?>
