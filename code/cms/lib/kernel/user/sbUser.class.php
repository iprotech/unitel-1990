<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbUser.
 *
 * @package    sbphp
 * @subpackage user
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbUser{

    private static $_culture,$_instance,$_authenticated;

    private function  __construct() {
        global $_system;
        if(!self::getAttribute("culture")){
            self::$_culture = $_system['default_culture'];
            self::setAttribute("culture",$_system['default_culture']);
        }else{
            self::$_culture = self::getAttribute("culture");
        }
        if(!self::getAttribute("authenticated")){
            self::$_authenticated = false;
            self::setAttribute("authenticated",false);
        }else{
            self::$_authenticated = self::getAttribute("authenticated");
        }
    }

    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new sbUser();
        }
        return self::$_instance;
    }

    public function getCulture(){
        return self::$_culture;
    }

    public function setCulture($name){
        $name = strtolower($name);
        self::$_culture = $name;
        self::setAttribute("culture",$name);
    }

    public function getAttribute($name){
        if(empty($_SESSION[$name])) return null;
        else return $_SESSION[$name];
    }

    public function setAttribute($name,$value){
        return $_SESSION[$name] = $value;
    }

    public function setAuthenticated($status){
        self::$_authenticated = $status;
        self::setAttribute("authenticated",$status);
    }

    public function getAuthenticated(){
        return self::$_authenticated;
    }

    public function clear(){
        session_destroy();
    }
    
}
?>
