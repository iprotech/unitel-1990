<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbView.
 *
 * @package    sbphp
 * @subpackage view
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbView{

    function  __construct() {
        
    }

    public static function includePartial($partialName,$variables){
        foreach($variables as $key=>$value) $$key = $value;
        $aPath = explode("/",$partialName);
        require_once "modules/".$aPath[0]."/templates/_".$aPath[1].".php";
    }
    
}
?>
