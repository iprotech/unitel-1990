<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbI18N.
 *
 * @package    sbphp
 * @subpackage view
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbI18N{

    function __construct(){

    }

    public function getLabelValue($label){
        global $_language;
        return $_language[$label];
    }

    public function loadLanguage(){
        
    }
    
}

?>
