<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbController.
 *
 * @package    sbphp
 * @subpackage controller
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbController{

    private $_template,$_variables,$_layout,$_layoutStatus,$_module,$_action,$_title,$_meta;
    public  $aRouting;

    function __construct(){
        $this->aRouting = sbRouting::parse();
        $this->_module =$this->aRouting['module'];
        $this->_action =$this->aRouting['action'];
	$this->_template = $this->_action;
        $this->_variables = array();
        $this->_layout = "layout";
        $this->_layoutStatus = true;
    }

    public function setLayoutStatus($status){
        $this->_layoutStatus = $status;
    }

    public function getLayoutStatus(){
        return $this->_layoutStatus;

    }

    public function setLayout($layout){
        $this->_layout = $layout;
    }

    public function getLayout(){
        return $this->_layout;
    }

    public function setTemplate($template){
        $this->_template = $template;
    }

    public function getTemplate(){
        return $this->_template;
    }
    

    public function draw(){
        $this->_variables = get_object_vars($this);
        foreach($this->_variables as $key=>$value) $$key = $value;
        require_once("modules/{$this->_module}/templates/{$this->_template}.php");
    }

    public function execute(){
        
    }

    public function getUser(){
        return sbUser::getInstance();
    }

    public function setTitle($title){
        $this->_title = $title;
    }

    public function getTitle(){
        return $this->_title;
    }

    public function setMeta($meta){
        return $this->_meta = $meta;
    }
    
    public function getMeta(){
        return $this->_meta;
    }
    
    public function redirect($url){
        header("location: {$url}")
        ?>
            <meta http-equiv="refresh" content="0 ; url=<?php echo $url; ?>">
        <?php
	exit();
    }
    
}

?>
