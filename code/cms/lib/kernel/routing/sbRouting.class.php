<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbRouting.
 *
 * @package    sbphp
 * @subpackage routing
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbRouting{

    private static $_aRouting;
    
    function __construct(){
        
    }

    public static function parse(){
        global $_system,$_iRouting;
        if(!self::$_aRouting){
            self::$_aRouting = array();
            $aParam = explode("?",$_SERVER['REQUEST_URI']);
            $aParam = explode("index.php",@$aParam[0]);
            $aParam = explode("/",@$aParam[1]);
            self::$_aRouting['module'] = strtolower(@$aParam[1]);
            self::$_aRouting['action'] = @$aParam[2];
            $queries = @$aParam[3];
            if($queries){
                $queries = explode("&", $queries);
                foreach($queries as $query){
                    $item = explode("-",$query);
                    @$_REQUEST[$item[0]]=@$item[1];
                }
            }
            //print_r($_iRouting[self::$_aRouting['module']]['action']);
            //var_dump($_iRouting[self::$_aRouting['module']]['action'][self::$_aRouting['action']]);
            if(@$_iRouting[self::$_aRouting['module']]['action'][self::$_aRouting['action']]){
                self::$_aRouting['action'] = $_iRouting[self::$_aRouting['module']]['action'][self::$_aRouting['action']];
            }
            if(@$_iRouting[self::$_aRouting['module']]) self::$_aRouting['module'] = $_iRouting[self::$_aRouting['module']]['name'];
            
            if(!self::$_aRouting['module']){
                self::$_aRouting['module'] = sbInput::get("module","txt",$_system['default_module']);
            }
            if(!self::$_aRouting['action']){
                self::$_aRouting['action'] = sbInput::get("action","txt",self::$_aRouting['module']);
            }
            /*
            if(!file_exists("modules/".self::$_aRouting['module']."/actions/".self::$_aRouting['action'].".class.php")){
                self::$_aRouting['module'] = $_system['default_module'];
                self::$_aRouting['action'] = $_system['default_action'];
            }
             * 
             */
        }
        
        return self::$_aRouting;
    }
}
?>
