<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbConfig.
 *
 * @package    sbphp
 * @subpackage config
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbConfig{
    
    private static $_config=array();

    function  __construct() {
        
    }

    public static function add($parameters = array())
    {
        self::$_config = array_merge(self::$_config, $parameters);
    }

    public static function get($name, $default = null)
    {
        return isset(self::$_config[$name]) ? self::$_config[$name] : $default;
    }

    public static function set($name, $value)
    {
        self::$config[$name] = $value;
    }
    
}
?>
