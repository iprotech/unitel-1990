<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbLoader.
 *
 * @package    sbphp
 * @subpackage config
 * @author     thiendv@gmail.com
 * @version    1.0
 */

class sbLoader{
    
    function  __construct() {
    }

    public static function loadHelper($helper){
        $aHelper = explode("/", $helper);
        if($aHelper[0]=="global"){
            require_once "lib/kernel/helper/$aHelper[1]Helper.php";
        }else{
            if(count($aHelper)>1){
                $path = $helper.".class.php";
            }else{
                $path = $helper."/C".strtoupper(substr($helper, 0, 1)).substr($helper,1).".class.php";
            }
            require_once "lib/helper/$path";
        }
    }

    public static function loadJavascript($name){
        echo '<script src="/web/js/'.$name.'.js" type="text/javascript"></script>';
    }

    public static function loadCss($name){
        echo '<link rel="stylesheet" type="text/css" href="/web/css/'.$name.'.css" />';
    }
}
?>
