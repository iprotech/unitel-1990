<?php
	function paging($totalPage, $currentPage, $minPage, $previousPage, $nextPage, $maxPage)
	{
	    $minPage = ($currentPage - 3 > 1 ? ($currentPage - 3) : 1);
	    $maxPage = ($currentPage + 3 < $totalPage ? ($currentPage + 3) : $totalPage);
	    $previousPage = $currentPage - 1;
	    $nextPage = $currentPage + 1;
	}

	function showPage($totalPage, $page, $url)
	{
	    if ($totalPage > 1)
	    {
	        $minPage = 0;
	        $maxPage = 0;
	        $previousPage = 0;
	        $nextPage = 0;
	        paging($totalPage, $page, &$minPage, &$previousPage, &$nextPage, &$maxPage);
	
	        $urlToFirst = sprintf($url, 1);
	        $urlToPrevious = sprintf($url, $previousPage);
	        $urlToNext = sprintf($url, $nextPage);
	        $urlToLast = sprintf($url, $totalPage);
	
	        if ($minPage == 1)
	        {
	            $linkToFirst = '';
	        }
	        else
	        if ($minPage == 2)
	        {
	            $linkToFirst = "<li class='fl'><a href='{$urlToFirst}'>1</a></li>";
	        }
	        else
	        {
	            $linkToFirst = "<li class='fl' ><a href='{$urlToFirst}'>1</a></li><li class='fl' style='padding-right:5px'>...</li>";
	        }
	
	        //$linkToPrevious = ($previousPage < 1 ? '' : link_to(PAGE_PREVIOUS, $urlToPrevious));
	
	        if ($maxPage == $totalPage)
	        {
	            $linkToLast = '';
	        }
	        else
	        if ($maxPage == $totalPage - 1)
	        {
	            $linkToLast = "<li class='fl' ><a href='{$urlToLast}'>{$totalPage}</a> </li>";
	        }
	        else
	        {
	            $linkToLast = '<li class="fl" style="padding-right:5px">...</li><li class="fl" >'."<a href='{$urlToLast}'>{$totalPage}</a></li>";
	        }
	
	        //$linkToNext = ($nextPage > $totalPage ? '' : link_to(PAGE_NEXT, $urlToNext));
	        $linkToPage = '';
	        for ($i = $minPage; $i <= $maxPage; $i++)
	        {
	            $urlToPage = sprintf($url, $i);
	            $linkToPage.=($i == $page ?'<li class="fl" ><a class="pagingS">'.$i.'</a></li>' :'<li class="fl" >'."<a href='{$urlToPage}'>{$i}</a>".'</li>'.'');
	        }
	        echo  '
	                     <ul>   
	                         '.$linkToFirst.                         
                                 $linkToPage.
                                 $linkToLast.'
	                     </ul>
	               ';        
	    }
	}
	
	function showPage_1($totalPage, $page, $url)
	{
	    if ($totalPage > 1)
	    {
	        $minPage = 0;
	        $maxPage = 0;
	        $previousPage = 0;
	        $nextPage = 0;
	        paging($totalPage, $page, &$minPage, &$previousPage, &$nextPage, &$maxPage);
	
	        $urlToFirst = sprintf($url, 1);
	        $urlToPrevious = sprintf($url, $previousPage);
	        $urlToNext = sprintf($url, $nextPage);
	        $urlToLast = sprintf($url, $totalPage);
	
	        if ($minPage == 1)
	        {
	            $linkToFirst = '';
	        }
	        else
	        if ($minPage == 2)
	        {
	            $linkToFirst = '<a href="'.$urlToFirst.'">1</a>';//link_to(1, $urlToFirst);
	        }
	        else
	        {
	            $linkToFirst = '<a href="'.$urlToFirst.'">1</a>...';//link_to(1, $urlToFirst).'...';
	        }
	
	        //$linkToPrevious = ($previousPage < 1 ? '' : link_to(PAGE_PREVIOUS, $urlToPrevious));
	
	        if ($maxPage == $totalPage)
	        {
	            $linkToLast = '';
	        }
	        else
	        if ($maxPage == $totalPage - 1)
	        {
	            $linkToLast = '<a href="'.$urlToLast.'">'.$totalPage.'</a>';//link_to($totalPage, $urlToLast);
	        }
	        else
	        {
	            $linkToLast = '<li>...</li><li><a href="'.$urlToLast.'">'.$totalPage.'</a>';//link_to($totalPage, $urlToLast).'</li>';
	        }
	
	        //$linkToNext = ($nextPage > $totalPage ? '' : link_to(PAGE_NEXT, $urlToNext));
	        $linkToPage = '';
	        for ($i = $minPage; $i <= $maxPage; $i++)
	        {
	            $urlToPage = sprintf($url, $i);
	            $linkToPage.=($i == $page ?'<li><a class="pagingS">'.$i.'</a></li>' :'<li><a href="'.$urlToPage.'">'.$i.'</a></li>'.'');
	        }
	        echo  '<div>Trang: '.
	        '
	                     <ul>   
	                         <li>'.$linkToFirst.'</li>'.                         
	        $linkToPage.
	        '<li>'.$linkToLast.'</li>
	                     </ul>
	                   </div>
	               ';        
	    }
	}
	
	function showPageAjax($totalPage, $page, $updateZone, $url,$loadingZone='')
	{
	    if ($totalPage > 1)
	    {
	        $minPage = 0;
	        $maxPage = 0;
	        $previousPage = 0;
	        $nextPage = 0;
	        paging($totalPage, $page, &$minPage, &$previousPage, &$nextPage, &$maxPage);
	
	        $urlToFirst = sprintf($url, 1);
	        $urlToPrevious = sprintf($url, $previousPage);
	        $urlToNext = sprintf($url, $nextPage);
	        $urlToLast = sprintf($url, $totalPage);
	        //$linkToFirst = ($minPage == 1 ? '' : link_to_remote(1, array('update' => $updateZone, 'url' => $urlToFirst)).'...');
	        if ($minPage == 1)
	        {
	            $linkToFirst = '';
	        }
	        else
	        if ($minPage == 2)
	        {
	            $linkToFirst = link_to_remote(1, array('update' => array('success'=>$updateZone,'failure'=>'error'),'loading'=> "Element.show('{$loadingZone}')", 'url' => $urlToFirst, 'script'=>true));
	        }
	        else
	        {
	            $linkToFirst = link_to_remote(1, array('update' => array('success'=>$updateZone,'failure'=>'error'),'loading'=> "Element.show('{$loadingZone}')", 'url' => $urlToFirst,'script'=>true)).'<li class="fl" style="padding-right:5px">...</li>';
	        }
	
	        if ($maxPage == $totalPage)
	        {
	            $linkToLast = '';
	        }
	        else
	        if ($maxPage == $totalPage - 1)
	        {
	            $linkToLast = link_to_remote($totalPage, array('update' => array('success'=>$updateZone,'failure'=>'error'),'loading'=> "Element.show('{$loadingZone}')", 'url' => $urlToLast, 'script'=>true));
	        }
	        else
	        {
	            $linkToLast = '<li class="fl" style="padding-right:5px">...</li><li class="fl">'.link_to_remote($totalPage, array('update' => array('success'=>$updateZone,'failure'=>'error'),'loading'=> "Element.show('{$loadingZone}')", 'url' => $urlToLast, 'script'=>true));
	        }
	
	        //$linkToNext = ($nextPage > $totalPage ? '' : link_to_remote(PAGE_NEXT, array('update' => $updateZone, 'url' => $urlToNext)));
	        $linkToPage = '';
	        for ($i = $minPage; $i <= $maxPage; $i++)
	        {
	            $urlToPage = sprintf($url, $i);
	            $linkToPage.=($i == $page ?'<li class="fl"><a class="pagingS">'.$i.'</a></li>' :'<li class="fl">'.link_to_remote($i, array('update' => array('success'=>$updateZone,'failure'=>'error'),'loading'=> "Element.show('{$loadingZone}')", 'url' => $urlToPage, 'script'=>true)).'</li>'.'');
	        }
	        echo  '
	                     <ul>                                    
	                         <li class="fl">'.$linkToFirst.'</li>'.
	        					$linkToPage.
	                        '<li class="fl">'.$linkToLast.'</li>
	                     </ul>
	               
	               ';        
	    }
	}
        function link_to_remote($name, $option){
            print_r($option);
            return "<a onclick='new Ajax.Updater({success:'".$option['update']['success']."',failure:'{$option['update']['failure']}'}, '{$option['url']}', {asynchronous:true, evalScripts:true, onLoading:function(request, json){{{$option['update']['loading']}}}});; return false;' href='#'>{$name}</a>";
        }
?>