<?php
/****************************************************************************
 *	Copyright (C) 2007 thiendv@gmail.com. All Rights Reserved.
 *	The following is Sample Code and is subject to all restrictions on
 *	such code as contained in the End User License Agreement accompanying
 *	this product.
 ****************************************************************************/
 
class CUri {
	
	/*-- start Method section --*/
	
	/*
	 *	Scope: Public
	 *	Level: Instance
	 *	Constructor
	 */
	function __construct( ) {
		
	}
	
	
	/*
	 *	Scope: Public
	 *	Level: Class
	 */
	public static function redir( $url ) {
		?>
		<meta http-equiv="refresh" content="0 ; url=<?php echo $url; ?>">
		<?php
		exit();
	}
	
	
	/*
	 *	Scope: Public
	 *	Level: Class
	 */
	public static function jsRedir( $url ) {
		?>
		<script language="javascript">
		document.location.href = "<?php echo $url; ?>";
		</script>
		<?php
		exit();
	}
	
	
	/*
	 *	Scope: Public
	 *	Level: Class
	 */
	public static function getRootPath( ) {
		$sRealPath = realpath( './' ) ;
	
		$sSelfPath = $_SERVER['PHP_SELF'] ;
		$sSelfPath = substr( $sSelfPath, 0, strrpos( $sSelfPath, '/' ) ) ;
	
		return substr( $sRealPath, 0, strlen( $sRealPath ) - strlen( $sSelfPath ) ) ;
	}
        
	

        /*
	 *	Scope: Public
	 *	Level: Class
         *      Build uri from soucre
	 */
         public static function buildUri($aParam){
             $queryString = "";
             $i = 0;
             if(!empty($aParam['query'])){
                 foreach($aParam['query'] as $key=>$value){
                     if(!$i){
                        $queryString .= "?";
                     }
                     $i++;
                     $queryString .= "$key=$value";
                     if($i<count($aParam['query'])){
                         $queryString .= "&";
                     }
                 }
             }
             $url = "/index.php";
             if(!empty($aParam['module'])) $url .= "/{$aParam['module']}";
             if(!empty($aParam['action'])) $url .= "/{$aParam['action']}";
             return $url.$queryString;
         }

         public static function url($module,$action,$query="",$seo=""){
             global $_routing;
             if(@$_routing[$module]){
                 if(@$_routing[$module]["action"][$action]) $action = $_routing[$module]["action"][$action];
                 $module = $_routing[$module]["name"];
             }
             $url = "/index.php/$module";
             if($action) $url .= "/$action";
             if($query) $url .= "/".  str_replace("=","-",$query);
             if($seo) $url.= "/$seo";
             
             return $url;
         }
        /*-- end Method section --*/
	
}


?>