<?php

/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * I18NHelper.
 *
 * @package    sbphp
 * @subpackage helper
 * @author     thiendv@gmail.com
 * @version    1.0
 */

function __($label){
    $aRouting = sbRouting::parse();
    $culture  = strtolower(sbUser::getInstance()->getCulture());
    $path = "modules/{$aRouting['module']}/i18n/messages.{$culture}.php";
    if(file_exists($path)){
        include $path;
        if(@$_languages[$label]) return $_languages[$label];
    }
    include("i18n/messages.{$culture}.php");
    return @$_languages[$label];
}


?>
