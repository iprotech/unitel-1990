<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbInput.
 *
 * @package    sbphp
 * @subpackage helper
 * @author     thiendv@gmail.com
 * @version    1.0
 */
 
class  sbInput
{
	function __construct()
	{
	}
	
	
	public static function get( $varname , $type = ''  , $vartext = '' , $method = '')
	{
		global $HTTP_POST_VARS, $HTTP_GET_VARS, $HTTP_COOKIE_VARS, $_REQUEST;
		
		$value	=	$vartext;
		
		if ( isset( $_POST[ $varname ] ) )
		{
			$value	= 	$_POST[ $varname ];
		}
		else if ( isset($_GET[ $varname ]) )
		{
			$value 	= 	$_GET[ $varname ];
		}
		else if( isset($_REQUEST[ $varname ] ) )
		{
			$value	=	$_REQUEST[ $varname ];
		}
		else if( isset($_FILES[ $varname ] ) )
		{
			$value	=	$_FILES[ $varname ];
		}
		
		switch ( $type )
		{
			case 'html':
				$value = sbInput::html($value);
				break;
			case 'txt':
				$value = sbInput::def( $value );
				break;
			case 'int':
				$value = sbInput::cint( $value );
				break;
			case 'sql':
				$value = sbInput::csql( $value );
				break;
			default:
				$value = sbInput::cstr( $value );
				break;
		}
		
		return $value;
	}
	
	
	function cstr( $strval )
	{
		if ( !get_magic_quotes_gpc() ) $strval = addslashes($strval);
		//$strval = addslashes($strval);
		
		if(strlen($strval))
			$strval = htmlspecialchars($strval);
			
		return $strval;
	}
	
	function html( $str ) {
		if ( !get_magic_quotes_gpc() ) $str = addslashes($str);
		return $str;
	}
	
	function def( $strval )
	{
		if ( !get_magic_quotes_gpc() ) $strval = addslashes($strval);
		//$strval = addslashes($strval);
		//$strval = htmlspecialchars($strval);
		
		return $strval;
	}
	
	
	function csql( $strval )
	{
		if ( !get_magic_quotes_gpc()  ) $strval = addslashes($strval);
		//$strval = addslashes($strval);
		return $strval;
	}
	
	
	function cint( $intval )
	{
		$intval = (int) $intval;
		
		return $intval;
	}
	
	
	function toSeoName($string)
	{
		$seoUrl = trim($string);		
		$seoUrl = preg_replace('/\s+/', '-', $seoUrl); // Replace all white space
		$pattern = "/[^-a-z0-9A-Z,]/";
		$seoUrl = preg_replace( $pattern,"",$seoUrl );						
		return $seoUrl;
	}

        
	function toSeoNameTag($string)
	{
		$seoUrl = trim($string);		
		$seoUrl = preg_replace('/\s+/', '', $seoUrl); // Replace all white space
		$pattern = "/[^-a-z0-9A-Z,]/";
		$seoUrl = preg_replace( $pattern,"",$seoUrl );						
		return $seoUrl;
	}
	
}
?>