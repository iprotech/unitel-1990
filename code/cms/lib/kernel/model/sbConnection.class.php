<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbConnection.
 *
 * @package    sbphp
 * @subpackage model
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbConnection {

    private static $_instance;
    private static $_conn;
    
    private function __construct(){
        
    }

    public static function connect(){
        return self::$_conn;
    }

    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new sbConnection();
        }
        return self::$_instance;
    }

    
    public function getConnection($_connection=""){
        global $_dbcon,$_system;
        
        if(!$_connection){
            $_connection = $_system['default_conn'];
        }
        
        if(self::$_conn[$_connection]){
            return self::$_conn[$_connection]['conn'];
        }else{
            switch($_dbcon[$_connection]['db_type']){
                case 'mssql':
                    self::$_conn[$_connection]['conn'] = new sbMssqlADO($_dbcon[$_connection]['server'],$_dbcon[$_connection]['username'],$_dbcon[$_connection]['password'],$_dbcon[$_connection]['database']);
                    self::$_conn[$_connection]['name'] = $_connection;
                    break;
                default:
                    self::$_conn[$_connection]['conn'] = new sbMysqlPDO($_dbcon[$_connection]['server'],$_dbcon[$_connection]['username'],$_dbcon[$_connection]['password'],$_dbcon[$_connection]['database']);
                    self::$_conn[$_connection]['name'] = $_connection;
                    self::$_conn[$_connection]['conn']->doSelect(" SET NAMES 'utf8' ");
                    break;
            }
        }

        return self::$_conn[$_connection]['conn'];
    }

    
}
?>
