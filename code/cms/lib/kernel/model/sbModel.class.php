<?php

/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbModel.
 *
 * @package    sbphp
 * @subpackage model
 * @author     thiendv@gmail.com
 * @version    1.0
 */

class sbModel{
	
    protected $table,$conn;

    function __construct($table,$conn){
        $this->table = $table;
        $this->conn = $conn;
    }

    public function retrieveByPK($pkName,$pkValue){
        $sql = "SELECT * FROM {$this->table} WHERE {$pkName} = '{$pkValue}' ";
        return $this->conn->doSelectOne($sql);
    }

    public function add($data){
        if(is_array($data)){
            $sqlInsert = "INSERT INTO {$this->table}";
            $sqlField = "(";
            $sqlValue = "VALUES(";
            $i = 0;
            foreach($data as $key=>$value){
                    if($i){
                            $sqlField .= ",".$key;
                            $sqlValue .= ",'".$value."'";
                    }else{
                            $sqlField .= $key;
                            $sqlValue .= "'".$value."'";
                            $i++;
                    }
            }
            $sqlField .= ")";
            $sqlValue .= ")";
            $sqlInsert .= $sqlField.$sqlValue;
            //echo $sqlInsert;
            //exit();
            $this->conn->doUpdate($sqlInsert);
            return $this->conn->getLastInsertId();
        }
    }

    
    public function update($data,$conndition){
        if(is_array($data)){
            $i=0;
            $sqlField = "";
            foreach($data as $key=>$value){
                if($i){
                        $sqlField .= ",".$key."='".$value."'";
                }else{
                        $sqlField .= $key."='".$value."'";
                        $i++;
                }
            }

            $sql = "UPDATE {$this->table} set {$sqlField} WHERE {$conndition} ";
            //echo $sql;
            return $this->conn->doUpdate($sql);
        }else{
            return false;
        }
    }

    public function delete($condition){
        $sql = "DELETE FROM {$this->table} WHERE {$condition} ";
        return $this->conn->doUpdate($sql);
    }
	
	
}
?>