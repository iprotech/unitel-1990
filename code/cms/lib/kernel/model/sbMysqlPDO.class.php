<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbMysqlPDO.
 *
 * @package    sbphp
 * @subpackage model
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbMysqlPDO{
   
   private $con,$error;
   
   function __construct($hostname,$username,$password,$dbname){
       $this->error = array();
       try{
           $this->con = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$password");
           $this->con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
       }catch(PDOException $e){
           $this->error[] = $e->getMessage();
           //echo $e->getMessage();
       }
   }

   public function getConnType(){
       return "mysql";
   }

   public function doSelect($sql){
       try{
           $stmt = $this->con->prepare($sql);
           $stmt->execute();
           return $stmt->fetchAll();
       }catch(PDOException $e){
          $this->error[] = array("query"=>$sql,"error"=>$e->getMessage());
          //echo $e->getMessage();
          return false;
       }
   }

   public function doSelectOne($sql){
       try{
           $stmt = $this->con->prepare($sql);
           $stmt->execute();
           $data = $stmt->fetchAll();
           if($data) return $data[0];
       }catch(PDOException $e){
           $this->error[] = array("query"=>$sql,"error"=>$e->getMessage());
           //echo $e->getMessage();
           return false;
       }
   }

   public function getLastInsertId(){
       return $this->con->lastInsertId();
   }

   public function doUpdate($sql){
       try{
           $this->con->exec($sql);
           if($this->getLastInsertId()) return $this->getLastInsertId();
           else return true;
       }catch(PDOException $e){
           $this->error[] = array("query"=>$sql,"error"=>$e->getMessage());
           //echo $e->getMessage();
           return false;
       }
   }

   public function getConnection(){
       return $this->con;
   }
   
   public function getError(){
       return $this->error;
   }
   
   public function closeConnection(){
       $this->con = NULL;
   }
}

?>