<?php
/*
 * This file is part of the sbphp package.
 * (c) 2004-2006 thiendv@gmail.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sbPortal.
 *
 * @package    sbphp
 * @subpackage portal
 * @author     thiendv@gmail.com
 * @version    1.0
 */
class sbPortal{

    protected $moduleName,$actionName,$module;
    
    function  __construct(){
        $aRouting = sbRouting::parse();
        $this->moduleName = $aRouting['module'];
        $this->actionName = $aRouting['action'];
        $this->loadModule($this->moduleName,$this->actionName);
        $this->module = new $this->actionName();
    }

    public function run(){
        $this->module->execute();
        if($this->module->getLayoutStatus()){
            $this->getLayout($this->module->getLayout());
        }else{
            $this->module->draw();
        }
    }

    public function getContent(){
        $this->module->draw();
    }

    public function loadModule($module,$action){
        require_once("modules/{$module}/actions/{$action}.class.php");
    }

    public function getTitle(){
        return $this->module->getTitle();
    }

    public function getMeta(){
        return $this->module->getMeta();
    }
    
    public function getLayout($layout="layout"){
        $portal = $this;
        require_once "web/layout/{$layout}.php";
    }
    
}
?>
