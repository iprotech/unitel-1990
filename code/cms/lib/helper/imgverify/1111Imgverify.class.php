<?php
/*

	The author makes no warranty about this module or any harms may cause to your system.
	This library requires PHP GD2 extension installed
*/
class Imgverify
{
	// publics
	public $image       = null;
	public $randString  = "";
	public $basePath    = "data/img.verify";
	public $fontName    = "";
	public $fontDir     = "fonts";
	public $imageDir    = "images";
	public $outImageDir = "vimages";

	// statics
	static $nRandChars  = 4;  // number of characters
	static $charSpacing = 5; // pixels

	// privates
	protected $chars    = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUWVXYZ";
	protected $imgFile  = "";
	
	function VerificationImage()
	{
		$this->basePath = "lib/helper/imgverify";
	}
	// create a random image
	function Create($width, $height)
	{
		$this->imageDir = $this->basePath."/".$this->imageDir;
		$this->fontDir  = $this->basePath."/".$this->fontDir;
		//$this->outImageDir = $this->basePath."/".$this->outImageDir;
		$this->outImageDir = "data/images";

		// remove old files
		$this->RemoveImage();

		// try to load a random image
		$imageName = $this->GenerateRandImageName();
		$this->image = @imagecreatefrompng($imageName);// or die("Cannot load image ".$imageName);

		// no then create it
		if (!$this->image)
		{
			//could be this imagecreatetruecolor but it might take up more space
			$this->image = @imagecreate($width, $height) or die("Cannot Initialize new GD image stream");
			$background_color = imagecolorallocate($this->image, 255, 255, 255);
		}

		$text_color       = imagecolorallocate($this->image, 100, 100, 100);

		$fontSize      = 0;
		$fontHeight    = $height*0.6; // 70% of image height
		$fontMinHeight = $fontHeight-1;
		$fontName      = $this->GenerateRandFontName();
		$left          = 10;                  // x as starting point (pixels)
		$angle         = 0;

		// Set the enviroment variable for GD
		// putenv('GDFONTPATH=' . realpath('.'));

		for ($i=0; $i<self::$nRandChars; $i++)
		{
			// generate random factors
			$randChar = $this->GenerateRandChar();
			$angle    = $this->GenerateRandAngle(20);
			$fontSize = $this->GenerateRandFontSize($fontMinHeight, $fontHeight);
			$bottom   = intval($fontSize)+5; // y as baseline

			// draw image
			$bbox = @imagettftext($this->image, $fontSize, $angle, $left, $bottom,  $text_color, $fontName, $randChar);
			if (!$bbox)
			{
				$fontName = $this->fontDir."/"."arial.ttf";
				imagettftext($this->image, $fontSize, $angle, $left, $bottom,  $text_color, $fontName, $randChar);
			}

			$bbox     = imagettfbbox($fontSize, $angle, $fontName, $randChar);
			$left    += ($bbox[2]-$bbox[0])+self::$charSpacing;
			$this->randString .= $randChar;
		}

		// store this file path for later use
		$this->imgFile       = $this->outImageDir."/".md5($this->randString).".png";

		// return values
		$result              = array();
		$result["code"]      = $this->randString;
		$result["imagePath"] = $this->imgFile;

		imagepng($this->image, $result["imagePath"]);
		imagedestroy($this->image);

		return $result;
	}

	function GenerateRandImageName()
	{
		$items = scandir($this->imageDir);
		$max   = count($items) - 2; // ignore . and .. by default
		$rand  = rand(2, $max);
		return $this->imageDir."/".$items[$rand];
	}

	function GenerateRandFontName()
	{
		$items = scandir($this->fontDir);
		$max   = count($items) - 2; // ignore . and .. by default
		$rand  = rand(2, $max);
		//echo $items[$rand]."<br/>";
		return $this->fontDir."/".$items[$rand];
	}

	function GenerateRandAngle($margin=10)
	{
		$sign = (rand(0, 10)%2==0)?1:-1;
		return $sign*rand(0, $margin);
	}

	function GenerateRandChar()
	{
		$sampleLen = strlen($this->chars);
		$pos  = rand(0, $sampleLen-1);
		//echo "Pos ".$pos."<br/>";
		$char = $this->chars{$pos};
		return $char;
	}

	function GenerateRandFontSize($min=5, $max=12)
	{
		$rand = rand($min, $max);
		return $rand;
	}

	function RemoveImage()
	{
		// remove old images after 2 mins by default
		$items = scandir($this->outImageDir);
		$max   = count($items) - 2; // ignore . and .. by default
		$time  = time();
		for ($i = 2; $i < $max; $i++)
		{
			$fPath = $this->outImageDir."/".$items[$i];
			$handle = fopen($fPath, "r");
			if (!$handle) continue;
			$stat = fstat($handle);
			fclose($handle);
			$ctime = $stat["ctime"];
			$tspan = ($time-$ctime)/60;
			//echo $tspan."<br/>";
			try
			{
				if ($tspan>=2) { unlink($fPath); }
			}
			catch (Exception $error)
			{
				//
			}
		}
	}
}
?>
