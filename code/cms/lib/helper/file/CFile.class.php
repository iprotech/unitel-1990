<?php
/****************************************************************************
 *	Copyright (C) 2007 thiendv@gmail.com. All Rights Reserved.
 *	The following is Sample Code and is subject to all restrictions on
 *	such code as contained in the End User License Agreement accompanying
 *	this product.
 ****************************************************************************/

class CFile {

	/*-- start Method section  --*/

	/*
	 *	Scope: Public
	 *	Level: Instance
	 *	Constructor
	 */
	function CFile( ) {

	}


	/*
	 *	Scope: Public
	 *	Level: Class
	 */
	public static function uploadFile( $file_name, $folder, $aTypeUpload = "" )
	{
		$temp_file = $_FILES[$file_name]["tmp_name"];
                $file_name = str_replace(" ","",$_FILES[$file_name]["name"]);
                if( empty( $aTypeUpload ) ) $typeUpload= array( "jpg", "jpeg", "png", "gif", "bmp" );
		else $typeUpload = $aTypeUpload;

		if(
			!in_array(strtolower(substr($file_name, -3)), $typeUpload) &&
			!in_array(strtolower(substr($file_name, -4)), $typeUpload)
		) return '';
		$sOriginalFileName = $file_name ;
		$sExtension = substr( $file_name, ( strrpos($file_name, '.') + 1 ) ) ;
		$sExtension = strtolower( $sExtension ) ;
		$iCounter = 0 ;
		$sServerDir = $folder."/";
		while ( true )
		{
			// Compose the file path.
			$sFilePath = $sServerDir . str_replace(" ","",$file_name) ;

			// If a file with that name already exists.
			if ( is_file( $sFilePath ) )
			{
				$iCounter++ ;
				$file_name = CFile::removeFileExtension( $sOriginalFileName ) . '_' . $iCounter . '.' . $sExtension ;
			}
			else
			{
				move_uploaded_file( $temp_file, $sFilePath ) ;
				if ( is_file( $sFilePath ) )
				{
					$oldumask = umask(0) ;
					chmod( $sFilePath, 0777 ) ;
					umask( $oldumask ) ;
				}
				clearstatcache();
				break ;
			}
			clearstatcache();
		}

		return $file_name;
	}


	/*
	 *	Scope: Public
	 *	Level: Class
	 */
	public static function removeFileExtension( $fileName )
	{
            return substr( $fileName, 0, strrpos( $fileName, '.' ) ) ;
	}

        public static function getFileExtension($fileName){
            return substr( $fileName,(strrpos( $fileName, '.' )+1) ) ;
        }


	/*
	 *	Scope: Public
	 */
	function unzip(  ) {
		$iParNum = func_num_args( );
		$aPar = func_get_args( );

		$sPackagePath = $aPar[0]; // absolute path to zip file
		$sDesDir = $aPar[1]; // Path/Path2/Path3/

		CFile::makeDirFromString( $sDesDir );
		$vZip = zip_open( $sPackagePath );

		if( $vZip ) {

			while( $vZipEntry = zip_read( $vZip ) ) {

				$sFileName = zip_entry_name( $vZipEntry );

				if( strrpos( $sFileName, "/" ) != ( strlen( $sFileName ) - 1 ) ) {
					$sFilePath = $sDesDir . $sFileName;
					$sDirPath = substr( $sFilePath, 0, strrpos( $sFilePath, "/" ) );
					CFile::makeDirFromString( $sDirPath  );
					zip_entry_open( $vZip, $vZipEntry, "r" );
					$sSize = zip_entry_filesize( $vZipEntry ) + 1;
					$vBuf = zip_entry_read( $vZipEntry, $sSize );
					$vFp = fopen( $sFilePath, "a+" );
					fwrite( $vFp, $vBuf );
					fclose( $vFp );
					zip_entry_close( $vZipEntry );
				}

			}

			zip_close( $vZip );

		}

	}


	/*
	 *	Scope: Public
	 */
	function makeDirFromString( ) {
		$iParNum = func_num_args( );
		$aPar = func_get_args( );

		if( $iParNum == 1 ) $sPath = $aPar[0];	# Path1/Path2/Path3/

		$aPath = explode( "/", $sPath );
		$sNewPath = "";
		foreach( $aPath as $v ) {
			$sNewPath .= $v;
			if( !in_array( $v, array( "", ".", ".." ) ) && !is_dir( $sNewPath ) ) {
				mkdir( $sNewPath, 0777 );
			}
			$sNewPath .= "/";
		}

	}


	/*
	 *	Scope: Public
	 */
	function copyDir( ) {
		$iParNum = func_num_args( );
		$aPar = func_get_args( );

		$sSrcDir = $aPar[0]; // Path1/Path2/Path3
		$sDesDir = $aPar[1]; // Path1/Path2/Path3
		if( $iParNum == 3 ) $sFPatern = $aPar[2]; // File name pattern (perl)

		$vDirHandle  = opendir( $sSrcDir );
		$aFiles = array();
		$aDirs = array();
		$aNotChosenFiles = array( ".", ".." );
		$aNotChosenDirs = array( ".", ".." );
		while ( false !== ( $sFileName = readdir( $vDirHandle ) ) ) {
			if( is_dir( $sSrcDir . "/" . $sFileName ) && !in_array( $sFileName, $aNotChosenDirs ) ) {
				$aDirs[] = $sFileName;
			}
			else if( !in_array( $sFileName, $aNotChosenFiles ) ) {
				if( $iParNum == 3 ) {
					if( preg_match( $sFPattern, $sFileName ) ) { $aFiles[] = $sFileName; }
				} else {
					$aFiles[] = $sFileName;
				}
			}
		}

		if( count( $aFiles ) > 0 ) {
			foreach( $aFiles as $k => $v ) {
				CFile::makeDirFromString( $sDesDir );
				copy( $sSrcDir . "/" . $v, $sDesDir . "/" . $v );
			}
		}

		foreach( $aDirs as $v ) {
			if( $iParNum == 3 )
				CFile::copyDir( $sSrcDir . "/" . $v, $sDesDir . "/" . $v, $sFPatern );
			else
				CFile::copyDir( $sSrcDir . "/" . $v, $sDesDir . "/" . $v );
		}

		closedir( $vDirHandle );
	}


	/*
	 *	Scope: Public
	 */
	function removeDir( ) {
		$iParNum = func_num_args( );
		$aPar = func_get_args( );

		$sSrcDir = $aPar[0]; // Path1/Path2/Path3
		if( $iParNum == 2 ) $sFPatern = $aPar[1]; // File name pattern (perl)

		$vDirHandle  = opendir( $sSrcDir );
		$aFiles = array();
		$aDirs = array();
		$aNotChosenFiles = array( ".", ".." );
		$aNotChosenDirs = array( ".", ".." );
		while ( false !== ( $sFileName = readdir( $vDirHandle ) ) ) {
			if( is_dir( $sSrcDir . "/" . $sFileName ) && !in_array( $sFileName, $aNotChosenDirs ) ) {
				$aDirs[] = $sFileName;
			}
			else if( !in_array( $sFileName, $aNotChosenFiles ) ) {
				if( $iParNum == 2 ) {
					if( preg_match( $sFPattern, $sFileName ) ) { $aFiles[] = $sFileName; }
				} else {
					$aFiles[] = $sFileName;
				}
			}
		}

		if( count( $aFiles ) > 0 ) {
			foreach( $aFiles as $k => $v ) {
				unlink( $sSrcDir . "/" . $v );
			}
		}

		foreach( $aDirs as $v ) {
			if( $iParNum == 2 )
				CFile::removeDir( $sSrcDir . "/" . $v, $sFPatern );
			else
				CFile::removeDir( $sSrcDir . "/" . $v );
		}

		closedir( $vDirHandle );
		rmdir( $sSrcDir );
	}


	// path input without '/' at last
	// return array of file names order by file name asc
	function getFilesFromDir() {
		$iParNum = func_num_args( );
		$aPar = func_get_args( );

		$sSrcDir = $aPar[0]; // Path1/Path2/Path3
		if( $iParNum == 2 ) $sFPatern = $aPar[1]; // File name pattern (perl)

		if( !is_dir( $sSrcDir ) ) return array();

		$vDirHandle  = opendir( $sSrcDir );
		$aFiles = array();
		$aDirs = array();
		$aNotChosenFiles = array( ".", ".." );
		$aNotChosenDirs = array( ".", ".." );
		while ( false !== ( $sFileName = readdir( $vDirHandle ) ) ) {
			if( is_file( $sSrcDir . "/" . $sFileName ) && !in_array( $sFileName, $aNotChosenFiles ) ) {
				if( $iParNum == 2 ) {
					if( preg_match( $sFPatern, $sFileName ) ) {
						$aFiles[] = $sFileName;
					}
				}
			}
		}

		closedir( $vDirHandle );
		sort( $aFiles );
		return $aFiles;
	}


	function getFileContent($filePath) {
		if(function_exists("file_get_contents")) {
			return file_get_contents($filePath);
		} else {
			$handle = fopen($filename, "r");
			$contents = fread($handle, filesize($filename));
			fclose($handle);
			return $contents;
		}
	}

	function putFileContent($filePath,$content){
		if(function_exists("file_put_contents")){
			return file_put_contents($filePath,$content);
		}else{
			$handle = fopen($filePath, "w");
			fwrite($filePath,$content);
			fclose($handle);
		}
	}

        public static function getPathStorage($rootDir, $itemId, $itemPerDir, $makeDir)
        {
            $dir1 = floor($itemId / ($itemPerDir * $itemPerDir * $itemPerDir));
            $dir2 = floor(($itemId - $dir1 * $itemPerDir * $itemPerDir * $itemPerDir)/($itemPerDir * $itemPerDir));
            $temp = floor($itemId / ($itemPerDir * $itemPerDir));
            $dir3 = floor(($itemId -  $temp * $itemPerDir * $itemPerDir)/$itemPerDir);

            $path = $dir1 . "/" . $dir2 . "/" .$dir3;

            if (!file_exists($rootDir. "/" . $path) && ($makeDir))
            {
                    if (!file_exists($rootDir. "/" . $dir1 . "/" . $dir2))
                    {
                            if (!file_exists($rootDir. "/" . $dir1))
                            {
                                    @mkdir($rootDir. "/" . $dir1, true);
                            }
                            @mkdir($rootDir. "/" . $dir1 . "/" . $dir2, true);
                            chmod($rootDir. "/" . $dir1, 0777 );
                    }
                    @mkdir($rootDir. "/" . $dir1 . "/" . $dir2, true);
                    chmod($rootDir. "/" . $dir1 . "/" . $dir2, 0777 );
            }
            @mkdir($rootDir. "/" . $dir1 . "/" . $dir2 . "/" . $dir3, true);
            chmod($rootDir. "/" . $dir1 . "/" . $dir2 . "/" . $dir3, 0777 );
            return $path;
        }

	/*-- end Method section  --*/

}
?>