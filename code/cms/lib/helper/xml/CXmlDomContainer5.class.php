<?php
/****************************************************************************
 *	Copyright (C) 2007 thiendv@gmail.com. All Rights Reserved.
 *	The following is Sample Code and is subject to all restrictions on
 *	such code as contained in the End User License Agreement accompanying
 *	this product.
 ****************************************************************************/
 
class CXmlDomContainer5 {
	
	var $m_oDom;
	var $m_sXmlVersion;
	var $m_sXmlEncoding;
	
	function CXmlDomContainer5( ) {
		$iParNum = func_num_args( );
		$aPar = func_get_args( );
		
		switch( $iParNum ) {
			case 0:
				$this->sXmlVersion = "1.0";
				$this->sXmlEncoding = "UTF-8";
				break;
			case 2:
				$this->sXmlVersion = $aPar[0];
				$this->sXmlEncoding = $aPar[1];
				break;
		}
		
		$this->m_oDom = new DOMDocument( $this->sXmlVersion, $this->sXmlEncoding );
	}
	
	
	function checkXmlParsing( $p_bRes ) {
		if( !$p_bRes ) die( "Error occurs in parsing xml document." );
	}
	
	
	function loadXmlFromUri( $p_sUri ) {
		$bRes = $this->m_oDom->load( $p_sUri );
		$this->checkXmlParsing( $bRes );
	}
	
	
	function loadXmlData( $p_sXml ) {
		$bRes = $this->m_oDom->loadXML( $p_sXml );
		$this->checkXmlParsing( $bRes );
	}
	
	
	function rootElement( ) {
		return $this->m_oDom->documentElement;
	}
	
	
	function firstChild( $p_oNode ) {
		return $p_oNode->firstChild;
	}
	
	
	function lastChild( $p_oNode ) {
		return $p_oNode->lastChild;
	}
	
	
	function childNodes( $p_oNode ) {
		$aTemp = array( );
		$oDomNodeList = $p_oNode->childNodes;
		for( $iC = 0; $iC < $oDomNodeList->length; $iC++ ) {
			$aTemp[] = $oDomNodeList->item( $iC );
		}
		return $aTemp;
	}
	
	
	function nextSibling( $p_oNode ) {
		return $p_oNode->nextSibling;
	}
	
	
	function previousSibling( $p_oNode ) {
		return $p_oNode->previousSibling;
	}
	
	
	function getText( $p_oNode ) {
		return $p_oNode->textContent;
	}
	
	
	function getAttribute( $p_oNode, $p_sAttName ) {
		return $p_oNode->getAttribute( $p_sAttName );
	}
	
	
	function getAttributes( $p_oNode ) {
		$aTemp = array( );
		$oDomNodeList = $p_oNode->attributes;
		for( $iC = 0; $iC < $oDomNodeList->length; $iC++ ) {
			$oDomAtt = $oDomNodeList->item( $iC );
			$sAttName = $oDomAtt->name;
			$sAttValue = $oDomAtt->value;
			$aTemp[$sAttName] = $sAttValue;
		}
		return $aTemp;
	}
	
	
	function getElementByTagName( $p_oNode, $p_sTagName ) {
		$oDomNodeList = $p_oNode->getElementsByTagName( $p_sTagName );
		for( $iC = 0; $iC < $oDomNodeList->length; $iC++ ) {
			$oDomNode = $oDomNodeList->item( $iC );
			if( $oDomNode->nodeName == $p_sTagName ){
				return $oDomNode;	
			}
		}
		return false;
	}
	
	
	function getElementsByTagName( $p_oNode, $p_sTagName ) {
		$aTemp = array( );
		$oDomNodeList = $p_oNode->getElementsByTagName( $p_sTagName );
		for( $iC = 0; $iC < $oDomNodeList->length; $iC++ ) {
			$oDomNode = $oDomNodeList->item( $iC );
			if( $oDomNode->nodeName == $p_sTagName ){
				$aTemp[] = $oDomNode;	
			}
		}
		return $aTemp;
	}
	
}

?>