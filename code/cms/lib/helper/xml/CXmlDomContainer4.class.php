<?php
/****************************************************************************
 *	Copyright (C) 2007 thiendv@gmail.com. All Rights Reserved.
 *	The following is Sample Code and is subject to all restrictions on
 *	such code as contained in the End User License Agreement accompanying
 *	this product.
 ****************************************************************************/
 
class CXmlDomContainer4 {
	
	var $m_oDom;
	var $m_sXmlVersion;
	var $m_sXmlEncoding;
	
	function CXmlDomContainer4( ) {
		if( !extension_loaded( "domxml" ) ) {
			$prefix = ( PHP_SHLIB_SUFFIX === 'dll' ) ? 'php_' : '';
			dl( $prefix . 'sqlite.' . PHP_SHLIB_SUFFIX ) 
			or die( "Cannot load <div style=\"font-weight:bold;\">domxml</div> extension." );
		}
	}
	
	
	function checkXmlParsing( ) {
		if( !$this->m_oDom ) die( "Error occurs in parsing xml document." );
	}
	
	
	function loadXmlFromUri( $p_sUri ) {
		$this->m_oDom = domxml_open_file( $p_sUri );
		$this->checkXmlParsing( );
	}
	
	
	function loadXmlData( $p_sXml ) {
		$this->m_oDom = domxml_open_mem( $p_sXml );
		$this->checkXmlParsing( );
	}
	
	
	function rootElement( ) {
		return $this->m_oDom->document_element( );
	}
	
	
	function firstChild( $p_oNode ) {
		return $p_oNode->first_child( );
	}
	
	
	function lastChild( $p_oNode ) {
		return $p_oNode->last_child( );
	}
	
	
	function childNodes( $p_oNode ) {
		return $p_oNode->child_nodes( );
	}
	
	
	function nextSibling( $p_oNode ) {
		return $p_oNode->first_child( );
	}
	
	
	function previousSibling( $p_oNode ) {
		return $p_oNode->previous_sibling( );
	}
	
	
	function getText( $p_oNode ) {
		return $p_oNode->get_content( );
	}
	
	
	function getAttribute( $p_oNode, $p_sAttName ) {
		return $p_oNode->get_attribute( $p_sAttName );	
	}
	
	
	function getAttributes( $p_oNode ) {
		$aTemp = array( );
		$aAtt = $p_oNode->attributes( );	
		foreach( $aAtt as $oAttNode ) {
			$sAttName = $oAttNode->name( );
			$sAttValue = $oAttNode->value( );
			$aTemp[$sAttName] = $sAttValue;
		}
		return $aTemp;
	}
	
	
	function getElementByTagName( $p_oNode, $p_sTagName ) {
		$aElement = $p_oNode->get_elements_by_tagname( $p_sTagName );
		foreach( $aElement as $oNode ) {
			if( $oNode->node_name( ) == $p_sTagName ) {
				return $oNode;
			}
		}
		return false;
	}
	
	
	function getElementsByTagName( $p_oNode, $p_sTagName ) {
		return $p_oNode->get_elements_by_tagname( $p_sTagName );
	}
	
}

?>