<?php
/****************************************************************************
 *	Copyright (C) 2007 thiendv@gmail.com. All Rights Reserved.
 *	The following is Sample Code and is subject to all restrictions on
 *	such code as contained in the End User License Agreement accompanying
 *	this product.
 ****************************************************************************/
 
require_once( "CXmlDomContainer4.class.php" );
require_once( "CXmlDomContainer5.class.php" );

class CXmlDomContainer {
	
	var $m_oDom;
	var $m_sXmlVersion;
	var $m_sXmlEncoding;
	
	
	
	/*
	 *	Scope: Public
	 *	Level: Instance
	 *	Contructor
	 */
	function CXmlDomContainer( ) {
		
	}
	
	
	/*
	 *	Scope: Public
	 *	Level: Class
	 */
	function get( ) {
		$aTemp = explode( ".", PHP_VERSION );
		switch( true ) {
			case ( $aTemp[0] <= 4 ):
				return new CXmlDomContainer4( );
				break;
			case ( $aTemp[0] >= 5 ):
				return new CXmlDomContainer5( );
				break;
		}
	}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 */
	function checkXmlParsing( $p_bRes ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Load XML document from file, use real path
	 */
	function loadXmlFromUri( $p_sUri ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Load XML document from string
	 */
	function loadXmlData( $p_sXml ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get root element of XML document
	 */
	function rootElement( ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get first child node of a node
	 */
	function firstChild( $p_oNode ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get last child node of a node
	 */
	function lastChild( $p_oNode ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get all child nodes of a node
	 */
	function childNodes( $p_oNode ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get next sibling node of a node
	 */
	function nextSibling( $p_oNode ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get previous sibling node of a node
	 */
	function previousSibling( $p_oNode ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get text of a node
	 */
	function getText( $p_oNode ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get sepcified attribute value of a node
	 */
	function getAttribute( $p_oNode, $p_sAttName ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get array attributes of a node (array['attName' => 'attValue',...])
	 */
	function getAttributes( $p_oNode ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get array child nodes specified by tag name of a node
	 */
	function getElementsByTagName( $p_oNode, $p_sTagName ) {}
	
	
	/*
	 *	Score: Public
	 *	Level: Instance
	 *	Get first child node specified by tag name of a node
	 */
	function getElementByTagName( $p_oNode, $p_sTagName ) {}
}

?>