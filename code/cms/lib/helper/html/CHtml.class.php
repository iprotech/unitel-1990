<?php
/****************************************************************************
 *	Copyright (C) 2007 thiendv@gmail.com. All Rights Reserved.
 *	The following is Sample Code and is subject to all restrictions on
 *	such code as contained in the End User License Agreement accompanying
 *	this product.
 ****************************************************************************/
 
class CHtml {
	
	/*-- start Method section --*/
	
	/*
	 *	Scope: Public
	 *	Level: Instance
	 *	Constructor
	 */
	function CHtml( ) {
		
	}
	
	public function showPage($totalPage, $page, $url)
	{
	    if ($totalPage > 1)
	    {
	        $minPage = 0;
	        $maxPage = 0;
	        $previousPage = 0;
	        $nextPage = 0;
                $currentPage = $page;
                $minPage = ($currentPage - 3 > 1 ? ($currentPage - 3) : 1);
                $maxPage = ($currentPage + 3 < $totalPage ? ($currentPage + 3) : $totalPage);
                $previousPage = $currentPage - 1;
                $nextPage = $currentPage + 1;

	        $urlToFirst = sprintf($url, 1);
	        $urlToPrevious = sprintf($url, $previousPage);
	        $urlToNext = sprintf($url, $nextPage);
	        $urlToLast = sprintf($url, $totalPage);

	        if ($minPage == 1)
	        {
	            $linkToFirst = '';
	        }
	        else
	        if ($minPage == 2)
	        {
	            $linkToFirst = "<a href='{$urlToFirst}'>1</a>";
	        }
	        else
	        {
	            $linkToFirst = "<a href='{$urlToFirst}'>1</a><a>...</a>";
	        }

	        //$linkToPrevious = ($previousPage < 1 ? '' : link_to(PAGE_PREVIOUS, $urlToPrevious));

	        if ($maxPage == $totalPage)
	        {
	            $linkToLast = '';
	        }
	        else
	        if ($maxPage == $totalPage - 1)
	        {
	            $linkToLast = "<a href='{$urlToLast}'>{$totalPage}</a>";
	        }
	        else
	        {
	            $linkToLast = '<a>...</a>'."<a href='{$urlToLast}'>{$totalPage}</a>";
	        }

	        //$linkToNext = ($nextPage > $totalPage ? '' : link_to(PAGE_NEXT, $urlToNext));
	        $linkToPage = '';
	        for ($i = $minPage; $i <= $maxPage; $i++)
	        {
	            $urlToPage = sprintf($url, $i);
	            $linkToPage.=($i == $page ?'<a class="current">'.$i.'</a>' :"<a href='{$urlToPage}'>{$i}</a>");
	        }
                echo  $linkToFirst.$linkToPage.$linkToLast;
	    }else{
                echo '<a class="current">1</a>';
            }
	}

	function dropdownPaging( $p_iPageIndex, $p_iRecordsPerPage, $p_iTotalRecords, $p_sSelectBoxPageName, $p_sSelectBoxPageCallback, $p_sSelectBoxDisplayName = "", $p_sSelectBoxDisplayCallback = "" ) {
		$aRecordsPerPage = array( 1, 2, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 );
		if( !in_array( $p_iRecordsPerPage, $aRecordsPerPage ) ) $p_iRecordsPerPage = 25; 
		
		$iTotalPages = ceil( $p_iTotalRecords / $p_iRecordsPerPage );
		
		$sSelectBoxPage = "Page ";
		$sSelectBoxPage .= "<select id=\"{$p_sSelectBoxPageName}\" name=\"{$p_sSelectBoxPageName}\" onchange=\"{$p_sSelectBoxPageCallback}( );\">";
		for( $i = 1; $i <= $iTotalPages; $i++ ){
			if( $i == $p_iPageIndex ) 
				$sSelectBoxPage .= "<option value=".$i." selected>".$i."</option>";
			else 
				$sSelectBoxPage .= "<option value=".$i.">".$i."</option>";
		}
		$sSelectBoxPage .= "</select>";
		$sSelectBoxPage .= " of {$iTotalPages} pages.";
		
		$sSelectBoxDisplay = "Display ";
		$sSelectBoxDisplay .= "<select id=\"{$p_sSelectBoxDisplayName}\" name=\"{$p_sSelectBoxDisplayName}\" onchange=\"{$p_sSelectBoxDisplayCallback}( );\">";
		foreach( $aRecordsPerPage as $i ){
			if( $i == $p_iRecordsPerPage ) 
				$sSelectBoxDisplay .= "<option value=".$i." selected>".$i."</option>";
			else 
				$sSelectBoxDisplay .= "<option value=".$i.">".$i."</option>";
		}
		$sSelectBoxDisplay .= "</select>";
		$sSelectBoxDisplay .= " records per page.";
		
		$iStartRecordIndex = ( $p_iPageIndex - 1 ) * $p_iRecordsPerPage + 1;
		$iEndRecordIndex = ( $p_iPageIndex * $p_iRecordsPerPage < $p_iTotalRecords ) ? $p_iPageIndex * $p_iRecordsPerPage: $p_iTotalRecords;
		if( $p_iTotalRecords == 0 ) {
			$iStarRecordIndex = 0;
			$iEndRecordIndex = 0;
		}
		
		if( $p_iTotalRecords > 0 ) {
			$sResult = "Results {$iStartRecordIndex} - {$iEndRecordIndex} of {$p_iTotalRecords} records.";
		}
		else {
			$sResult = "Results none of records.";
		}
		
		return array( "page" => $sSelectBoxPage, "display" => $sSelectBoxDisplay, "result" => $sResult );
	}
	
	
	/*
	 *	Scope: Public
	 *	Level: Class
	 *	Create option tags
	 */
	function getOptionTags( ) {
		$iParNum = func_num_args();
		$aPar = func_get_args();
		
		global $oDb;
		
		$detectSelected = false;
		$selectedValues = array();
		$sOption = "";
		
		switch( $iParNum ) {
			case 3:
				$query = $aPar[0];
				$optionValue = $aPar[1];
				$optionText = $aPar[2];
				break;
			case 4:
				$query = $aPar[0];
				$optionValue = $aPar[1];
				$optionText = $aPar[2];
				$selectedValues = $aPar[3];
				
				$detectSelected = true;
				break;
			case 6:
				$query = $aPar[0];
				$optionValue = $aPar[1];
				$optionText = $aPar[2];
				$selectedValues = $aPar[3];
				
				$detectSelected = true;
				$sOption = "<option value=\"". $aPar[4] ."\">" . $aPar[5] . "</option>";
				break;
		}
		
		
		$sel = "";
		$iRs = $oDb->query( $query );
		while( $rc = $oDb->fetchArray( $iRs ) ) {
			if( $detectSelected ) {
				if( in_array( $rc[$optionValue], $selectedValues ) ) {
					$sel = "selected=\"selected\"";
				} else {
					$sel = "";
				}
			}
			$sOption .= "<option value=\"".$rc[$optionValue]."\" {$sel} >".$rc[$optionText]."</option>";
		}
		
		return $sOption;
	}
	
	
	function attachHTMLComment( $p_sCommentBlockName ) {
		$sCode = "\$sHTMLC_{$p_sCommentBlockName} = \"<!--\";";
		$sCode .= "\$eHTMLC_{$p_sCommentBlockName} = \"-->\";";
		
		return $sCode;
	}
	
	
	function detachHTMLComment( $p_sCommentBlockName ) {
		$sCode = "\$sHTMLC_{$p_sCommentBlockName} = \"\";";
		$sCode .= "\$eHTMLC_{$p_sCommentBlockName} = \"\";";
		
		return $sCode;
	}
	/*-- end Method section --*/
	
}
?>