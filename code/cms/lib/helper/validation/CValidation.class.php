<?php
/**
* Validate data
* Date: 06 Oct, 2008
*/

class CValidation {
	function __construct() {
	
	}
	
	/*
		return true if post
	*/
	public static function isRun() {
		if(count($_POST) > 0) {
			return true;
		}
		return false;
	}
	
	/*
		return true if parameter is a numeric (include Float and Integer numbers)
	*/
	public static function isNumeric($str) {
		return (bool)preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $str);
	}
	
	/*
		return true if parameter is an integer number
	*/
	public static function isInteger($str) {
		return (bool)preg_match( '/^[\-+]?[0-9]+$/', $str);
	}
	
	/*
		return true if paramter is a alpha string ( only include a->z characters)
	*/
	public static function isAlpha($str) {
		return ( ! preg_match("/^([a-z])+$/i", $str)) ? FALSE : TRUE;	
	}
	
	public static function isAlphaNumeric($str) {
		return ( ! preg_match("/^([a-z0-9])+$/i", $str)) ? FALSE : TRUE;	
	}
	
	public static function isAlphaDash($str) {
		return ( ! preg_match("/^([-a-z0-9_-])+$/i", $str)) ? FALSE : TRUE;	
	}
	
	public static function isEmpty($str) {
		if ( ! is_array($str)) {
			return (trim($str) == '') ? TRUE : FALSE;
		} else {
			return empty($str);
		}	
	}
	
	public static function inRange($str, $min, $max) {
		$len = strlen($str);

		if($len >= $min && $len <= $max) {
			return true;
		}
		return false;
	}
	
	public static function isEmail($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}	
	
	public static function isNotSpecialCharacters( $str ){
		return ( ! preg_match("/^([a-z0-9 ])+$/i", $str)) ? FALSE : TRUE;	
	}
	
	public static function isPostcode($postcode) {
		$postcode = strtoupper($postcode);
		if(ereg("((GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)"
				."|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)"
				."|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]"
				."|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))"
				."|[0-9][A-HJKS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})", $postcode)) {
			return $postcode;
		} else {
			return FALSE;
		}
	}

}

?>