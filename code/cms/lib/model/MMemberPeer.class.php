<?php

class MMemberPeer extends sbModel
{   
    function  __construct() {
        parent::__construct("member",sbConnection::getConnection());
    }
    
    public function doInsertMul($sql){
        $this->conn->doUpdate($sql);
    }
    
    public function getListMember($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM member
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT member.*
                    FROM member
                    WHERE 1=1 {$condition}
                    ORDER BY member.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
                    
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT member.*
                    FROM member
                    WHERE member.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
    
    public function checkExists($msisdn){
        $query = "
                    SELECT member.*
                    FROM member
                    WHERE msisdn='{$msisdn}' AND status=1
                 ";
        return $this->conn->doSelectOne($query);
    }
}
