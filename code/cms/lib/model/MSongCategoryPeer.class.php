<?php

class MSongCategoryPeer extends sbModel
{
    function  __construct(){
        parent::__construct("song_category",sbConnection::getConnection());
    }

    public function getAllCate(){
        return $this->conn->doSelect("SELECT * FROM song_category ORDER BY id DESC");
    }

    
    public function getListCate($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM song
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT song_category.*
                    FROM song_category
                    WHERE 1=1 {$condition}
                    ORDER BY song_category.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT song_category.*
                    FROM song_category
                    WHERE song_category.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
    
}