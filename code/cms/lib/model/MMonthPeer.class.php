<?php

class MMonthPeer extends sbModel
{
    function  __construct(){
        parent::__construct("horoscope_month",sbConnection::getConnection());
    }

    
    public function getListFile($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM horoscope_month
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT horoscope_month.*
                    FROM horoscope_month
                    WHERE 1=1 {$condition}
                    ORDER BY horoscope_month.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT horoscope_month.*
                    FROM horoscope_month
                    WHERE horoscope_month.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
    
}