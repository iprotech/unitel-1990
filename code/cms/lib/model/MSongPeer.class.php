<?php

class MSongPeer extends sbModel
{
    function  __construct(){
        parent::__construct("song",sbConnection::getConnection());
    }

    
    public function getListSong($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM song INNER JOIN song_category ON song.category_id=song_category.id
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT song.*,song_category.name as category_name
                    FROM song INNER JOIN song_category ON song.category_id=song_category.id
                    WHERE 1=1 {$condition}
                    ORDER BY song.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT song.*
                    FROM song
                    WHERE song.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
    
}