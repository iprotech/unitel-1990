<?php

class MReportPeer extends sbModel
{   
    function  __construct() {
        parent::__construct("report",sbConnection::getConnection());
    }

    public function getAll(){
        return $this->conn->doSelect("SELECT * FROM report ORDER BY id DESC ");
    }
    public function getList($condition='',$page=0,$recorPerPage=20){

        $sqlCount = "
                        SELECT count(*) as total FROM report
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT report.*
                    FROM report
                    WHERE 1=1 {$condition}
                    ORDER BY id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);

        $resultBlock = $this->conn->doSelectOne($sqlBlock);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems,
                        'totalBlock' => $resultBlock['total'],
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT report.*
                    FROM report
                    WHERE report.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
}
