<?php

class MVoiceFilePeer extends sbModel{
    function __construct(){
        parent::__construct("voice_file",sbConnection::getConnection());
    }

    public function getList($contentId){
        $files = $this->conn->doSelect("SELECT * FROM voice_file WHERE content_id=$contentId order by press_key ");
        $datas = array();
        foreach($files as $file){
            $datas[$file['press_key']] = $file;
        }
        return $datas;
    }
    
}

?>