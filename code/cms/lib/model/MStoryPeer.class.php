<?php

class MStoryPeer extends sbModel
{
    function  __construct(){
        parent::__construct("story",sbConnection::getConnection());
    }

    
    public function getListStory($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM story INNER JOIN story_category ON story.category_id=story_category.id
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT story.*,story_category.name as category_name
                    FROM story INNER JOIN story_category ON story.category_id=story_category.id
                    WHERE 1=1 {$condition}
                    ORDER BY story.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT story.*
                    FROM story
                    WHERE story.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
    
}