<?php

class MCdrPeer extends sbModel
{   
    function  __construct() {
        parent::__construct("cdr",sbConnection::getConnection());
    }

    public function getAllSchedule(){
        return $this->conn->doSelect("SELECT * FROM cdr ORDER BY id DESC ");
    }
    public function getListCdr($condition='',$page=0,$recorPerPage=20){

        $sqlCount = "
                        SELECT count(*) as total FROM cdr
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT cdr.*
                    FROM cdr
                    WHERE 1=1 {$condition}
                    ORDER BY id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);


        $sqlBlock   = "
                        SELECT sum(ceil(duration/30)) as total FROM cdr WHERE dcontext!='voicebroadcast' AND accountcode NOT LIKE '%broadcast%' {$condition}
                    ";
        
        $resultBlock = $this->conn->doSelectOne($sqlBlock);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems,
                        'totalBlock' => $resultBlock['total'],
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT cdr.*
                    FROM cdr
                    WHERE cdr.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
}
