<?php

class MServicePeer extends sbModel
{
    function  __construct() {
        parent::__construct("service",sbConnection::getConnection());
    }

    public function getListService($condition='',$page=0,$recorPerPage=20){

        $sqlCount = "
                        SELECT count(*) as total FROM service
                        WHERE 1=1 {$condition}
                    ";

        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)* $recorPerPage;
        if(!$startOffset) $startOffset = 0;
        
        $query = "
                    SELECT * FROM service
                    WHERE 1 = 1 {$condition}
                    ORDER BY id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        //echo $query;
        $resultItems =  $this->conn->doSelect($query);
        
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getAllServices($condition=''){
        $query = "
                    SELECT * FROM service
                    WHERE 1=1 {$condition}
                    ORDER BY id DESC
                 ";
        //echo $query;
        //$this->conn->query($query);
        return $this->conn->doSelect($query);
    }
}
