<?php

class MAccountPeer extends sbModel
{   
    function __construct(){
        parent::__construct("account",sbConnection::getConnection());
    }

    public function checkLogin($username,$password){
        $query = "
                    SELECT * FROM account WHERE username = '{$username}' and password = '{$password}'
                 ";
        return $this->conn->doSelectOne($query);
    }

    public function checkExists($username){
        $query = "
                    SELECT * FROM account WHERE username = '{$username}'
                 ";
        return $this->conn->doSelectOne($query);
    }

    public function getListAccount($condition='',$page=0,$recorPerPage=20){

        $sqlCount = "
                        SELECT count(*) as total FROM account
                        WHERE 1=1 {$condition}
                    ";

        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)* $recorPerPage;
        if(!$startOffset) $startOffset = 1;
       
        $query = "
                    SELECT  * FROM account
                    WHERE
                    ORDER BY id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        //echo $query;
        $resultItems =  $this->conn->doSelect();
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }
}
