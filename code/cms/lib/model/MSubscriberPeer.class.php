<?php

class MSubscriberPeer extends sbModel
{   
    function  __construct() {
        parent::__construct("subscriber",sbConnection::getConnection());
    }
    public function doInsertMul($sql){
        $this->conn->doUpdate($sql);
    }
    public function getListSubscriber($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM subscriber
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT subscriber.*,schedule.name as schedule_name
                    FROM subscriber INNER JOIN schedule ON schedule.id=subscriber.schedule_id
                    WHERE 1=1 {$condition}
                    ORDER BY subscriber.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT subscriber.*
                    FROM subscriber
                    WHERE subscriber.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
}
