<?php

class MStoryCategoryPeer extends sbModel
{
    function  __construct(){
        parent::__construct("story_category",sbConnection::getConnection());
    }

    public function getAllCate(){
        return $this->conn->doSelect("SELECT * FROM story_category ORDER BY id DESC");
    }

    
    public function getListCate($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM story
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT story_category.*
                    FROM story_category
                    WHERE 1=1 {$condition}
                    ORDER BY story_category.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT story_category.*
                    FROM story_category
                    WHERE story_category.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
    
}