<?php
class MContentPeer extends sbModel
{
    function __construct(){
        parent::__construct("content",sbConnection::getConnection("sms"));
    }

    public function getListContent($condition='',$page=0,$recorPerPage=20){

        $sqlCount = "
                        SELECT count(*) as total FROM content
                        WHERE 1=1 {$condition}
                    ";

        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)* $recorPerPage;
        if(!$startOffset) $startOffset = 0;
        
        $query = "
                    SELECT content.*,service.service as service_name,account.username
                    FROM content LEFT JOIN service ON content.service_id=service.id
                    LEFT JOIN account ON content.creater_id = account.id
                    WHERE 1=1 {$condition}
                    ORDER BY content.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";

        $resultItems =  $this->conn->doSelect($query);
       

        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }
}

?>