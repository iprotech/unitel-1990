<?php

class MChargingHistoryPeer extends sbModel{

    function __construct(){
        parent::__construct("charging_history",sbConnection::getConnection());
    }

    public function getAllCharging($condition=''){
         $query = "
                        SELECT charging_history.*,member.msisdn
                        FROM charging_history INNER JOIN member ON member.id=charging_history.member_id
                        WHERE 1=1 {$condition}
                        ORDER BY charging_history.id DESC
                        LIMIT 0,1000
                        
                  ";
            return $this->conn->doSelect($query);
    }
    public function getListCharing($condition='',$page=0,$recorPerPage=20){

        $sqlRevenue = "
                        SELECT sum(charging_history.total_money) as total
                        FROM charging_history INNER JOIN member ON charging_history.member_id=member.id
                        WHERE 1=1 {$condition}
                    ";

        $resultRevenue = $this->conn->doSelectOne($sqlRevenue);

        $sqlCount = "
                        SELECT count(*) as total
                        FROM charging_history INNER JOIN member ON charging_history.member_id=member.id
                        WHERE 1=1 {$condition}
                    ";
                    
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)* $recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT charging_history.*,member.msisdn,member.status,member.cancel_date
                    FROM charging_history INNER JOIN member ON charging_history.member_id=member.id
                    WHERE 1=1 {$condition}
                    ORDER BY charging_history.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        //$this->conn->query($query);
        $resultItems =  $this->conn->doSelect($query);

        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems,
                        'totalRevenue' => $resultRevenue['total'],
                    );
    }

}

?>
