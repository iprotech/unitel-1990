<?php

class MSchedulePeer extends sbModel
{   
    function  __construct() {
        parent::__construct("schedule",sbConnection::getConnection());
    }

    public function getAllSchedule(){
        return $this->conn->doSelect("SELECT * FROM schedule ORDER BY id DESC ");
    }
    public function getListSchedule($condition='',$page=0,$recorPerPage=20){
        $sqlCount = "
                        SELECT count(*) as total FROM schedule
                        WHERE 1=1 {$condition}
                    ";
        $resultCount = $this->conn->doSelectOne($sqlCount);
        $totalRecord = $resultCount['total'];
        $totalPage = ceil($totalRecord/$recorPerPage);
        $currentPage = $page;
        $resultItems = array();

        if($page>$totalPage){
            $currentPage = $totalPage;
        }
        if($page<=1){
            $currentPage=1;
        }

        $startOffset = ($currentPage - 1)*$recorPerPage;
        if(!$startOffset) $startOffset = 0;
        $query = "
                    SELECT schedule.*,content.name as content_name,content.file_path
                    FROM schedule INNER JOIN content ON schedule.content_id = content.id
                    WHERE 1=1 {$condition}
                    ORDER BY schedule.id DESC
                    LIMIT $startOffset,$recorPerPage
                 ";
        $resultItems = $this->conn->doSelect($query);
        return array(
                        'totalPage'=> $totalPage,
                        'page'     => $currentPage,
                        'totalItem'=> $totalRecord,
                        'items'    => $resultItems
                    );
    }

    public function getDetail($id){
        $query = "
                    SELECT schedule.*
                    FROM schedule
                    WHERE schedule.id={$id}
                 ";
        return $this->conn->doSelectOne($query);
    }
}
