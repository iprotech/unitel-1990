<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Delete extends sbController{

    public function execute(){
        $this->id = sbInput::get("id","int");
        if($this->getUser()->getAuthenticated()&&($this->getUser()->getAttribute("group_id")==1)){
            $mAccount = new MAccountPeer();
            $mAccount->delete(("id={$this->id}"));
            $this->redirect("/index.php/account");
        }
    }
    
}
?>
