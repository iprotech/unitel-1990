<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Update extends sbController{

    public function execute(){
        if($this->getUser()->getAuthenticated()){
            $this->message = "";
            $this->update  = "";
            $this->id = sbInput::get("id","int");
            if(!$this->id&&($this->getUser()->getAttribute("group_id")!=1)){
                $this->id = $this->getUser()->getAttribute("id");
            }
            $mAccount = new MAccountPeer();
            $this->user = array();
            if($this->id){
                $this->user = $mAccount->retrieveByPK("id",$this->id);
                if(!$this->user) $this->redirect ("/index.php/account");
                $this->update = true;
                $username = sbInput::get("username","txt");
                $password = sbInput::get("password","txt");
                $groupId  = sbInput::get("group_id","int",$this->getUser()->getAttribute("group_id"));
                $this->groupId = $groupId;
                if($username&&$groupId){
                    $data = array(
                                    "username"=>$username,
                                    "group_id"=>$groupId
                                 );
                    if($password) $data["password"] = $password;
                    $mAccount->update($data,"id={$this->id}");
                    $this->message  = "This account has been updated successfully";
                    $this->username = $username;
                    $this->groupId  = $groupId;
                }
            }else{
                $username = sbInput::get("username","txt");
                $password = sbInput::get("password","txt");
                $groupId  = sbInput::get("group_id","int");
                $this->create = true;
                $data = array(
                                "username"=>$username,
                                "group_id"=>$groupId,
                                "password"=>$password
                             );
                if($username&&$groupId&&$password){
                    if(!$mAccount->checkExists($username)){
                        $mAccount->add($data);
                        $this->message = "User account has been created successfully";
                    }else{
                        $this->message = "The username has been exists, please choise an other.";
                    }
                }
            }
        }else{
            $this->redirect("/index.php/account/login");
            exit();
        }
    }
    
}
?>
