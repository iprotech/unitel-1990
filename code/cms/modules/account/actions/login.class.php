<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Login extends sbController{

    public function execute(){
        $this->setLayout("loginLayout");
        $this->error = false;
        $username = sbInput::get("username","txt");
        $password = sbInput::get("password","txt");
        $mAccount = new MAccountPeer();
        if($username&&$password){
            $user = $mAccount->checkLogin($username,$password);
            if($user){
                $this->getUser()->setAttribute("username",$username);
                $this->getUser()->setAttribute("id",$user['id']);
                $this->getUser()->setAttribute("group_id",$user['group_id']);
                $this->getUser()->setAuthenticated(true);
                $this->redirect("/index.php/cdr/cdr");
            }else{
                $this->error = "Username or password is not correct!";
            }
        }elseif($username||$password){
            $this->error = "Username or password is not correct!";
        }
    }
}

?>