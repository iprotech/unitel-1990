<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Logout extends sbController{

    public function execute(){
       $this->setLayout("loginLayout");
       $this->getUser()->clear();
       $this->redirect("/index.php/account/login");
    }
}
?>