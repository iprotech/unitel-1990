<script>
   $(document).ready(function() {
    $("#start_date").datepicker();
    $("#end_date").datepicker();
  });
</script>
<?php sbView::includePartial("account/leftmenu",array()); ?>
<div id="content" >
<div id="box">
        <h3>User management</h3>
        <table width="100%">
            <thead>
                <tr>
                    <th width="200px">Username</th>
                    <th>Group</th>
                    <th width="150px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($datas['items'] as $item): ?>
                <tr>
                    <td><?php echo $item['username']; ?></td>
                    <td><?php if($item['group_id']==1) echo "Administrator";else echo "CP";?></td>
                    <td>
                        <a href="/index.php/account/update?id=<?php echo $item['id']; ?>">Edit</a> |
                        <a href="/index.php/account/delete?id=<?php echo $item['id']; ?>" onclick="return confirm('Are you sure?');">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
    </table>
    <div id="pager">
        <?php
            if($url=="/index.php/sms?") $url .= "page=%d";
            else $url .= "&page=%d";
        ?>
        <span style="float:left">Pages &nbsp;</span>
        <?php sbLoader::loadHelper('html'); ?>
        <?php echo CHtml::showPage($datas['totalPage'],$page,$url); ?>
        Total <strong><?php echo $datas['totalItem']; ?></strong> records found
        <div style="clear:both"></div>
    </div>
</div>
</div>