<div id="box">
        <h3>Update account</h3>

    <form id="form" action="/index.php/account/update" method="get">
        <div style="margin: 10px 40px"><?php echo $message; ?></div>
        <label for="username">Username</label>
        <input type="text" id="username" name="username" <?php if($update) echo "readonly"; ?> value="<?php echo @$user['username']; ?>" />
        <br/>
        <label for="password">Password</label>
        <input type="password" id="password" name="password" /> (leave it blank if you don't want to change the password)
         <?php
            $group = sbConfig::get("group");
            $crGroupId = sbUser::getInstance()->getAttribute("group_id");
            $status = "";
        ?>
        <?php if($crGroupId==1): ?>
        <br/>
        <label for="group_id">Group</label>
        <select name="group_id" style="width:150px">
            <option value="1">Administrator</option>
            <option value="2">Customer care</option>
            <option value="3">Report</option>
        </select>
        <?php else: ?>
        <input type="hidden" name="group_id" value="<?php echo $groupId; ?>" />
        <?php endif; ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <br/>
        <div style="margin: 10px 40px">
            <input id="button1" type="submit" value="Submit" tabindex="6" />
            <input id="button2" type="Reset" tabindex="7" />
        </div>
    </form>
</div>