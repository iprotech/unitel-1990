<script>
   $(document).ready(function() {
    date_obj = new Date();
    date_obj_hours = date_obj.getHours();
    date_obj_mins = date_obj.getMinutes();

    if (date_obj_mins < 10) { date_obj_mins = "0" + date_obj_mins; }
    date_obj_time = " '"+date_obj_hours+":"+date_obj_mins+":00'";
    $("#start_time").datepicker({dateFormat: $.datepicker.W3C + date_obj_time});
    $("#end_time").datepicker({dateFormat: $.datepicker.W3C + date_obj_time});
  });
</script>
<div id="content_full">
<div id="box">
    <h3>CONFIG VOICE FILE</h3>
    <form id="form" action="/index.php/config/config" method="post" enctype="multipart/form-data">
        <input type="hidden" name="is_edit" value="true" />
        <?php if($inform): ?>
        <div style="margin: 10px 40px;color: green;font-weight: bold">Your modification has been updated successfully</div>
        <?php endif; ?>
        <table cellpadding="20" cellspacing="20">
            <?php if($error): ?>
            <tr>
                <td colspan="4">
                    <?php foreach($error as $ms): ?>
                    <p style="color:red;font-weight:bold"><?php echo $ms; ?></p>
                    <?php endforeach; ?>
                </td>
            </tr>
            <?php endif; ?>
            <?php foreach($files as $code=>$data): ?>
            <tr>
                <td width="300px">Code: <?php echo ++$code; ?> (<?php echo $data[1]; ?>)</td>
                <td>
                    <input type="file" name="<?php echo $data[0]; ?>" />
                    <?php if(is_file(sbConfig::get("voice_common").$data[0].".wav")): ?>
                    <br>
                    <object type="application/x-shockwave-flash" data="/flash/dewplayer-mini.swf?mp3=<?php echo "/".sbConfig::get("horoscope_mp3_file")."voice_common/".$data[0].".mp3"; ?>" width="160" height="20" id="dewplayer-mini">
                        <param name="wmode" value="transparent" />
                        <param name="movie" value="dewplayer-mini.swf?mp3=<?php echo "/".sbConfig::get("horoscope_mp3_file")."voice_common/".$data[0].".mp3"; ?>" />
                    </object>
                    <!--
                    <p>
                        <object width="260" height="65" viewastext="" type="application/x-oleobject" standby="Loading Microsoft Windows Media Player components..." codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715" classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95">
                            <param value="<?php echo "/".sbConfig::get("voice_common").$data[0].".wav"; ?>" name="FileName">
                            <param value="false" name="TransparentAtStart"><param value="false" name="AutoStart">
                            <param value="false" name="AnimationatStart"><param value="false" name="ShowControls">
                            <param value="false" name="ShowDisplay"><param value="1" name="playCount">
                            <param value="0" name="displaySize"><param value="100" name="Volume">
                            <param value="transparent" name="wmode">
                            <embed width="260" height="45" displaysize="0" wmode="transparent" volume="100" playcount="1" autostart="false" animationatstart="0" name="MediaPlayer" src="<?php echo "/".sbConfig::get("voice_common").$data[0].".wav"; ?>" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2">
                        </object>
                    </p>
                    -->
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>

        </table>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <input type="hidden" name="is_post" value="true" />
        <div style="margin: 10px 40px">
            <input id="button1" type="submit" value="Submit" tabindex="6" />
            <input id="button2" type="Reset" tabindex="7" />
        </div>
    </form>
</div>
</div>