<script>
   $(document).ready(function() {
        $("#start_charge_date").datepicker();
        $("#end_charge_date").datepicker();
  });
</script>


<div id="content_full" style="margin-top: 10px;">
<div id="box">
    <h3>REVENUE STATISTIC</h3>
    <form id="form" action="/index.php/statistic/statistic" method="get">
        <style>
            #form_filter{border:none}
            #form_filter td{border:none}
        </style>
        <fieldset id="address">
            <legend>Filter</legend>
            <table style="border:none;width:200px;" id="form_filter">
                <tr style="border:none;">
                    <td style="border:none;"><label for="phone">Phone number</label></td>
                    <td><input name="phone" id="phone" type="text" value="<?php echo $phoneNumber; ?>" tabindex="1" /></td>
                    <td><label for="song">Service</label></td>
                    <td>
                        <select name="service_id" id="service_id" class="w200">
                            <option></option>
                            <?php foreach($services as $service): ?>
                            <option value="<?php echo $service['id']; ?>" <?php if($serviceId==$service['id']) echo "selected" ?>>
                                <?php echo $service['service']; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="start_charge_date">Start charging date</label></td>
                    <td><input type="text" id="start_charge_date" name="start_charge_date" tabindex="3" value="<?php echo $startChargeDate; ?>" /></td>
                    <td><label for="end_charge_date">End charging date</label></td>
                    <td><input type="text" id="end_charge_date" name="end_charge_date" tabindex="4" value="<?php echo $endChargeDate; ?>" /></td>
                </tr>
            </table>
        </fieldset>

        <fieldset id="address">
            <legend>Revenue statistic </legend>
            <table>
                 <tr>
                     <td>Total revenue: <?php echo $datas['totalRevenue']; ?></td>
                 </tr>
            </table>
        </fieldset>

        <div align="left">
        <input id="button1" type="submit" value="View data" tabindex="6" />
        <input id="button2" type="Reset" tabindex="7" />
        </div>
    </form>
    <table width="100%">
        <thead>
            <tr>
                <th width="50px">ID</th>
                <th width="100px">Phone number</th>
                <th width="100px">Service</th>
                <th width="100px">Charging date</th>
                <th width="100px">Expried date</th>
                <th width="100px">Status</th>
                <th width="50px">Total money</th>
            </tr>
        </thead>
        <tbody>
            <?php $memberStatus = sbConfig::get("member_status"); ?>
            <?php foreach(@$datas['items'] as $item): ?>
            <tr>
                <td><?php echo $item['id']; ?></td>
                <td><?php echo $item['msisdn']; ?></td>
                <td><?php echo $item['service_name'];?></td>
                <td><?php echo date("d/m/Y H:i:s",  strtotime($item['start_charging_date'])); ?></td>
                <td><?php echo date("d/m/Y H:i:s",  strtotime($item['end_charging_date'])); ?></td>
                <td>
                    <?php echo $memberStatus[$item['status']]; ?>
                    <p>
                    <?php if($item['status']>2): ?>
                    <b><?php echo date("d/m/Y H:i:s",strtotime($item['cancel_date'])); ?></b>
                    <?php endif; ?>
                    </p>
                </td>
                <td>
                    <?php echo $item['total_money']; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div id="pager">
        <?php
            if($url=="/index.php/sms/mo?") $url .= "page=%d";
            else $url .= "&page=%d";
        ?>
        <span style="float:left">Pages &nbsp;</span>
        <?php sbLoader::loadHelper('html'); ?>
        <?php echo CHtml::showPage($datas['totalPage'],$page,$url); ?>
        Total <strong><?php echo $datas['totalItem']; ?></strong> records found
        <div style="clear:both"></div>
    </div>
</div>
</div>