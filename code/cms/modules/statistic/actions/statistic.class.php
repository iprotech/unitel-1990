<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Statistic extends sbController{

    public function execute(){

      
        $mCharge = new MChargingHistoryPeer();
        $mService = new MServicePeer();
        $this->services = $mService->getAllServices();
        $this->phoneNumber = sbInput::get("phone","txt");
        $this->serviceId   = sbInput::get("service_id","int");
        $this->startChargeDate   = sbInput::get("start_charge_date","txt");
        $this->endChargeDate     = sbInput::get("end_charge_date","txt");
       
        $this->url = "";
        $condition   = "";
        if($this->phoneNumber){
            $condition .= " AND msisdn LIKE '%{$this->phoneNumber}%' ";
            $this->url .= "&phone=".$this->phoneNumber;
        }
        if($this->startChargeDate){
            $condition .= " AND date(charging_history.start_charging_date)>= '".date('Y-m-d',strtotime($this->startChargeDate))."'";
            $this->url .= "&start_charge_date=".$this->startChargeDate;
        }
        if($this->endChargeDate){
            $condition .= " AND date(charging_history.start_charging_date) < '".date('Y-m-d',strtotime($this->endChargeDate))."'";
            $this->url .= "&end_charge_date=".$this->endChargeDate;
        }
        if($this->serviceId){
            $condition .= " AND member.service_id=".$this->serviceId;
            $this->url .= "&service_id=".$this->serviceId;
        }

        //echo $condition;
        $this->url = "/index.php/statistic/statistic?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mCharge->getListCharing($condition,$this->page,40);
    }

}
?>
