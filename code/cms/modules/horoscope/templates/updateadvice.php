<script>
   $(document).ready(function() {
    date_obj = new Date();
    date_obj_hours = date_obj.getHours();
    date_obj_mins = date_obj.getMinutes();

    if (date_obj_mins < 10) { date_obj_mins = "0" + date_obj_mins; }
    date_obj_time = " '"+date_obj_hours+":"+date_obj_mins+":00'";
    $("#start_time").datepicker({dateFormat: $.datepicker.W3C + date_obj_time});
    $("#end_time").datepicker({dateFormat: $.datepicker.W3C + date_obj_time});
    $("#start_date").datetimepicker();
    $("#end_date").datetimepicker();
  });
</script>
<div id="content_full">
<div id="box">
    <h3>Update advice</h3>
    <form id="form" action="/index.php/horoscope/updateadvice" method="post" enctype="multipart/form-data">
        <input type="hidden" name="is_edit" value="true" />
        <?php if($inform): ?>
        <div style="margin: 10px 40px;color: green;font-weight: bold">Your modification has been updated successfully</div>
        <?php endif; ?>
        <?php if($error): ?>
        <div style="margin: 10px 40px;color: red;font-weight: bold">Uploading file error</div>
        <?php endif; ?>
        <?php if($id): ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?php endif; ?>
        <label for="username">File name</label>
        <input type="text" id="name" name="name" value="<?php echo $voice['name']; ?>" />
        <br/>
        <br/>
        <label for="start_date">Start play date</label>
        <input type="text" id="start_date" name="play_date" tabindex="3" value="<?php echo $voice['start_play']; ?>" />
        <br/>
        <br/>
        <label for="end_date">End play date</label>
        <input type="text" id="end_date" name="end_date" tabindex="3" value="<?php echo $voice['end_play']; ?>" />
        <br/>
        
        <label for="voice_id">Voice File</label>
        <input type="file" name="voice_file" />
        <br/>
        <?php if($voice): ?>
        <div style="margin-left:110px;">
        <object type="application/x-shockwave-flash" data="/flash/dewplayer-mini.swf?mp3=<?php echo "/".sbConfig::get("horoscope_mp3_file")."advice/".  CFile::removeFileExtension($voice['file_path']).".mp3"; ?>" width="160" height="20" id="dewplayer-mini">
            <param name="wmode" value="transparent" />
            <param name="movie" value="dewplayer-mini.swf?mp3=<?php echo "/".sbConfig::get("horoscope_mp3_file")."advice/".  CFile::removeFileExtension($voice['file_path']).".mp3"; ?>" />
        </object>
        <br/>
        <object width="260" height="65" viewastext="" type="application/x-oleobject" standby="Loading Microsoft Windows Media Player components..." codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715" classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95">
            <param value="<?php echo "/".sbConfig::get("horoscope_file")."advice/".$voice['file_path']; ?>" name="FileName">
            <param value="false" name="TransparentAtStart"><param value="false" name="AutoStart">
            <param value="false" name="AnimationatStart"><param value="false" name="ShowControls">
            <param value="false" name="ShowDisplay"><param value="1" name="playCount">
            <param value="0" name="displaySize"><param value="100" name="Volume">
            <param value="transparent" name="wmode">
            <embed width="260" height="45" displaysize="0" wmode="transparent" volume="100" playcount="1" autostart="false" animationatstart="0" name="MediaPlayer" src="<?php echo "/".sbConfig::get("horoscope_file")."advice/".$voice['file_path']; ?>" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2">
        </object>
        </div>
        <?php endif; ?>
        <br/>
        <label for="status">Status</label>
        <select name="status" id="status">
            <option value="1" <?php if($voice['status']==1) echo "selected"; ?>>Publish</option>
            <option value="0" <?php if($voice['status']===0) echo "selected"; ?>>Private</option>
        </select>
        <br/>
        <div style="margin: 10px 40px">
            <input id="button1" type="submit" value="Submit" tabindex="6" />
            <input id="button2" type="Reset" tabindex="7" />
        </div>
    </form>
</div>
</div>