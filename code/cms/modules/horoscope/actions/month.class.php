<?php

class Month extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mMonth = new MMonthPeer();
        $this->name = sbInput::get("name","txt");
        $this->day = sbInput::get("day","int");

        $this->url = "";
        $condition   = "";

        if($this->name){
            $condition .= " AND horoscope_month.name LIKE '%{$this->name}%' ";
            $this->url .= "&name=".$this->name;
        }

        $this->url = CUri::url("horoscope","month")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mMonth->getListFile($condition,$this->page,40);
    }
}
?>