<?php

class Year2012 extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");

        $mVoiceAdvice = new MVoiceYear2012Peer();
        $this->name = sbInput::get("name","txt");
        $this->day = sbInput::get("day","int");

        $this->url = "";
        $condition   = "";

        if($this->name){
            $condition .= " AND horoscope_year2012.name LIKE '%{$this->name}%' ";
            $this->url .= "&name=".$this->name;
        }

        $this->url = CUri::url("horoscope","year2012")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mVoiceAdvice->getListFile($condition,$this->page,40);
    }
}
?>