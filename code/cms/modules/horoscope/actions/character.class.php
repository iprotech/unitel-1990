<?php

class Character extends sbController{

    public function  execute() {
        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        //$mVoice = new MConfigPeer();
        //Edit data
        $this->id = sbInput::get("id","int");
        $this->files = array(
                            "001"=>array("horo_1","01th"),
                            "002"=>array("horo_2","02th"),
                            "003"=>array("horo_3","03th"),
                            "004"=>array("horo_4","04th"),
                            "005"=>array("horo_5","05th"),
                            "006"=>array("horo_6","06th"),
                            "007"=>array("horo_7","07th"),
                            "008"=>array("horo_8","08th"),
                            "009"=>array("horo_9","09th"),
                            "010"=>array("horo_10","10th"),
                            "011"=>array("horo_11","11th"),
                            "012"=>array("horo_12","12th"),
                            "013"=>array("horo_13","13th"),
                            "014"=>array("horo_14","14th"),
                            "015"=>array("horo_15","15th"),
                            "016"=>array("horo_16","16th"),
                            "017"=>array("horo_17","17th"),
                            "018"=>array("horo_18","18th"),
                            "019"=>array("horo_19","19th"),
                            "020"=>array("horo_20","20th"),
                            "021"=>array("horo_21","21th"),
                            "022"=>array("horo_22","22th"),
                            "023"=>array("horo_23","23th"),
                            "024"=>array("horo_24","24th"),
                            "025"=>array("horo_25","25th"),
                            "026"=>array("horo_26","26th"),
                            "027"=>array("horo_27","27th"),
                            "028"=>array("horo_28","28th"),
                            "029"=>array("horo_29","29th"),
                            "030"=>array("horo_30","30th"),
                            "031"=>array("horo_31","31th"),
                           );
        if(sbInput::get("is_post","str")){
            //Upload file
            foreach($this->files as $code=>$data){
                //echo $code."|".$data[0]."<br/>";
                if($_FILES[$data[0]]['name']){
                    $uploadFile = CFile::uploadFile($data[0],sbConfig::get("horoscope_file")."character",array("wav","gsm"));
                    if($uploadFile){
                        //echo $uploadFile;
                        //echo sbConfig::get("voice_character").$uploadFile;
                        //echo sbConfig::get("voice_character").$data[0].".wav";
                        rename(sbConfig::get("horoscope_file")."character/".$uploadFile,sbConfig::get("horoscope_file")."character/".$data[0].".wav");
                    }else{
                        if($_FILES[$data[0]]['name']){
                            $this->error[$data[0]] = "Upload Code: $code error, plz check again!";
                        }
                    }
                }
            }
        }

    }

}

?>