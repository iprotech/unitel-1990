<?php

class Updatedaily extends sbController{

    public function  execute() {
        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mVoice = new MVoiceDailyPeer();
        

        //Edit data
        if(sbInput::get("is_edit","str")){
            $data = array();
            $data['name'] = sbInput::get("name","str");
            $data['day_in_week'] = sbInput::get("day","int");
            $data['status']   = sbInput::get("status","int");
            $data['created_datetime'] = date("Y-m-d H:i:s",time());
            if(sbInput::get("start_play","str")) $startDate = date("Y-m-d",strtotime(sbInput::get("start_play","str")));
            else $startDate = date("Y-m-d",time()-3*24*3600);
            $startDate = date("Y-m-d",time()-3*24*3600);
            $data['start_play'] = $startDate;
            
            if(sbInput::get("end_play","str")) $endDate = date("Y-m-d",strtotime(sbInput::get("end_play","str")));
            else $endDate = date("Y-m-d",time()+3*24*3600);
            $endDate = date("Y-m-d",time()+3*24*3600);
            $data['end_play'] = $endDate;
            
            $data['convert_status'] = 0;
            //Upload file
            $file = CFile::uploadFile("voice_file",sbConfig::get("horoscope_file")."daily/",array("wav","gsm"));
            //rename(sbConfig::get("horoscope_file")."daily/".$file,sbConfig::get("horoscope_file")."daily/".str_replace(" ","",$file));
            $id = sbInput::get("id","int");
            if($file){
                $fileStatus = file_get_contents(sbConfig::get("horoscope_file")."daily/".$file);
                if(!$fileStatus){
                    $this->redirect(CUri::url("horoscope","updatedaily","id=".$id."&error=true"));
                    exit();
                }else{
                    $orgPath = sbConfig::get("horoscope_file")."daily/".$file;
                    $destPath = sbConfig::get("horoscope_mp3_file")."daily/".CFile::removeFileExtension($file).".mp3";
                    exec("/usr/bin/ffmpeg -i {$orgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$destPath}");
                    $data['file_path'] = $file;
                }
            }
            if($id){
                $mVoice->update($data,"id=".$id);
                $item = null;
            }else{
                $id = $mVoice->add($data);
            }
            $this->redirect(CUri::url("horoscope","updatedaily","id=".$id."&inform=true"));
        }
        $this->inform = sbInput::get("inform","str");
        $this->error = sbInput::get("error","str");
        $id = sbInput::get("id","int");
        if($id){
            $this->id = $id;
            $this->voice = $mVoice->retrieveByPK("id",$id);
        }

    }

}

?>