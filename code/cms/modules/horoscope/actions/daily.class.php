<?php

class Daily extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mVoiceDaily = new MVoiceDailyPeer();
        $this->name = sbInput::get("name","txt");
        $this->day = sbInput::get("day","int");

        $this->url = "";
        $condition   = "";

        if($this->name){
            $condition .= " AND horoscope_daily.name LIKE '%{$this->name}%' ";
            $this->url .= "&name=".$this->name;
        }

        if($this->day){
            $condition .= " AND horoscope_daily.day_in_week='{$this->day}' ";
            $this->url .= "&day=".$this->day;
        }

        $this->url = CUri::url("horoscope","daily")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mVoiceDaily->getListFile($condition,$this->page,40);
    }
}
?>