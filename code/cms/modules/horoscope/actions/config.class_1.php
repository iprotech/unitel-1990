<?php

class Config extends sbController{

    public function  execute() {
        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        //Edit data
        if(sbInput::get("is_post","str")){
            //Upload file
            $welcome = CFile::uploadFile("welcome",sbConfig::get("voice_common"),array("wav","gsm"));
            if($welcome){
                rename(sbConfig::get("voice_common").$welcome, sbConfig::get("voice_common")."welcome.wav");
            }else{
                if($_FILES['welcome']['name']){
                    $this->error['welcome'] = "Upload Code: 001 error,plz check again!";
                }
            }

            $main_menu = CFile::uploadFile("main_menu",sbConfig::get("voice_common"),array("wav","gsm"));
            if($main_menu){
                rename(sbConfig::get("voice_common").$main_menu, sbConfig::get("voice_common")."main_menu.wav");
            }else{
                if($_FILES['main_menu']['name']){
                    $this->error['main_menu'] = "Upload code 002 error,plz check again!";
                }
            }

            $game_rule = CFile::uploadFile("game_rule",sbConfig::get("voice_common"),array("wav","gsm"));
            if($game_rule){
                rename(sbConfig::get("voice_common").$game_rule, sbConfig::get("voice_common")."game_rule.wav");
            }else{
                if($_FILES['game_rule']['name']){
                    $this->error['game_rule'] = "Upload code 003 error,plz check again!";
                }
            }

            $rule_menu = CFile::uploadFile("rule_menu",sbConfig::get("voice_common"),array("wav","gsm"));
            if($rule_menu){
                rename(sbConfig::get("voice_common").$rule_menu, sbConfig::get("voice_common")."rule_menu.wav");
            }else{
                if($_FILES['rule_menu']['name']){
                    $this->error['rule_menu'] = "Upload code 004 error,plz check again!";
                }
            }

            $rank_menu = CFile::uploadFile("rank_menu",sbConfig::get("voice_common"),array("wav","gsm"));
            if($rank_menu){
                rename(sbConfig::get("voice_common").$rank_menu, sbConfig::get("voice_common")."rank_menu.wav");
            }else{
                if($_FILES['rank_menu']['name']){
                    $this->error['rank_menu'] = "Upload code 005 file error,plz check again!";
                }
            }

            $question = CFile::uploadFile("question",sbConfig::get("voice_common"),array("wav","gsm"));
            if($question){
                rename(sbConfig::get("voice_common").$question, sbConfig::get("voice_common")."question.wav");
            }else{
                if($_FILES['question']['name']){
                    $this->error['question'] = "Upload code 006 file error,plz check again!";
                }
            }

            $answer = CFile::uploadFile("answer",sbConfig::get("voice_common"),array("wav","gsm"));
            if($answer){
                rename(sbConfig::get("voice_common").$answer, sbConfig::get("voice_common")."answer.wav");
            }else{
                if($_FILES['answer']['name']){
                    $this->error['answer'] = "Upload code 007 file error,plz check again!";
                }
            }

            $true_voice = CFile::uploadFile("true_voice",sbConfig::get("voice_common"),array("wav","gsm"));
            if($true_voice){
                rename(sbConfig::get("voice_common").$true_voice, sbConfig::get("voice_common")."true_voice.wav");
            }else{
                if($_FILES['true_voice']['name']){
                    $this->error['true_voice'] = "Upload code 008 file error,plz check again!";
                }
            }

            $wrong_voice = CFile::uploadFile("wrong_voice",sbConfig::get("voice_common"),array("wav","gsm"));
            if($wrong_voice){
                rename(sbConfig::get("voice_common").$wrong_voice, sbConfig::get("voice_common")."wrong_voice.wav");
            }else{
                if($_FILES['wrong_voice']['name']){
                    $this->error['wrong_voice'] = "Upload code 009 file error,plz check again!";
                }
            }

            $report_voice = CFile::uploadFile("report_voice",sbConfig::get("voice_common"),array("wav","gsm"));
            if($report_voice){
                rename(sbConfig::get("voice_common").$report_voice, sbConfig::get("voice_common")."report_voice.wav");
            }else{
                if($_FILES['report_voice']['name']){
                    $this->error['report_voice'] = "Upload code 010 file error,plz check again!";
                }
            }

            $report_menu = CFile::uploadFile("report_menu",sbConfig::get("voice_common"),array("wav","gsm"));
            if($report_menu){
                rename(sbConfig::get("voice_common").$report_menu, sbConfig::get("voice_common")."report_menu.wav");
            }else{
                if($_FILES['report_menu']['name']){
                    $this->error['report_menu'] = "Upload code 011 file error,plz check again!";
                }
            }

            $top_day = CFile::uploadFile("top_day",sbConfig::get("voice_common"),array("wav","gsm"));
            if($top_day){
                rename(sbConfig::get("voice_common").$top_day, sbConfig::get("voice_common")."top_day.wav");
            }else{
                if($_FILES['top_day']['name']){
                    $this->error['top_day'] = "Upload code 012 file error,plz check again!";
                }
            }

            $top_week = CFile::uploadFile("top_week",sbConfig::get("voice_common"),array("wav","gsm"));
            if($top_week){
                rename(sbConfig::get("voice_common").$top_week, sbConfig::get("voice_common")."top_week.wav");
            }else{
                if($_FILES['top_week']['name']){
                    $this->error['top_week'] = "Upload code 013 file error,plz check again!";
                }
            }

            $top_month = CFile::uploadFile("top_month",sbConfig::get("voice_common"),array("wav","gsm"));
            if($top_month){
                rename(sbConfig::get("voice_common").$top_month, sbConfig::get("voice_common")."top_month.wav");
            }else{
                if($_FILES['top_month']['name']){
                    $this->error['top_month'] = "Upload code 014 file error,plz check again!";
                }
            }
            $phone = CFile::uploadFile("phone",sbConfig::get("voice_common"),array("wav","gsm"));
            if($phone){
                rename(sbConfig::get("voice_common").$phone, sbConfig::get("voice_common")."phone.wav");
            }else{
                if($_FILES['phone']['name']){
                    $this->error['phone'] = "Upload code 015 file error,plz check again!";
                }
            }
            $playtime = CFile::uploadFile("playtime",sbConfig::get("voice_common"),array("wav","gsm"));
            if($playtime){
                rename(sbConfig::get("voice_common").$playtime, sbConfig::get("voice_common")."playtime.wav");
            }else{
                if($_FILES['playtime']['name']){
                    $this->error['playtime'] = "Upload code 016 file error,plz check again!";
                }
            }
            $second = CFile::uploadFile("second",sbConfig::get("voice_common"),array("wav","gsm"));
            if($second){
                rename(sbConfig::get("voice_common").$second, sbConfig::get("voice_common")."second.wav");
            }else{
                if($_FILES['second']['name']){
                    $this->error['second'] = "Upload code 017 file error,plz check again!";
                }
            }
            $minute = CFile::uploadFile("minute",sbConfig::get("voice_common"),array("wav","gsm"));
            if($minute){
                rename(sbConfig::get("voice_common").$minute, sbConfig::get("voice_common")."minute.wav");
            }else{
                if($_FILES['minute']['name']){
                    $this->error['minute'] = "Upload code 018 file error,plz check again!";
                }
            }

            $chooseAnswer = CFile::uploadFile("choose_answer",sbConfig::get("voice_common"),array("wav","gsm"));
            if($chooseAnswer){
                rename(sbConfig::get("voice_common").$chooseAnswer, sbConfig::get("voice_common")."choose_answer.wav");
            }else{
                if($_FILES['choose_answer']['name']){
                    $this->error['choose_answer'] = "Upload code 019 file error,plz check again!";
                }
            }
            
        }
    }

}

?>