<?php

class Week extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }

        //$mWeek = new MWeekPeer();
        $this->name = sbInput::get("name","txt");
        $this->day = sbInput::get("day","int");

        $this->url = "";
        $condition   = "";

        if($this->name){
            $condition .= " AND horoscope_week.name LIKE '%{$this->name}%' ";
            $this->url .= "&name=".$this->name;
        }

        $this->url = CUri::url("horoscope","week")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        //$this->datas = $mWeek->getListFile($condition,$this->page,40);
    }
}
?>