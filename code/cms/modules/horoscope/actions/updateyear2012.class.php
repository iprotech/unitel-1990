<?php

class Updateyear2012 extends sbController{

    public function  execute() {
        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mVoice = new MVoiceYear2012Peer();
        

        //Edit data
        if(sbInput::get("is_edit","str")){
            $data = array();
            $data['name'] = sbInput::get("name","str");
            $data['status']   = sbInput::get("status","int");
            $data['convert_status']   = 0;
            $data['created_datetime'] = date("Y-m-d H:i:s",time());
            //Upload file
            $file = CFile::uploadFile("voice_file",sbConfig::get("horoscope_file")."year/",array("wav","gsm"));
            rename(sbConfig::get("horoscope_file")."year/".$file,sbConfig::get("horoscope_file")."year/".str_replace(" ","",$file));
            $id = sbInput::get("id","int");
            if($id){
                if($file) $data['file_path'] = str_replace(" ","",$file);
                $mVoice->update($data,"id=".$id);
                $item = null;
            }else{
                $data['file_path'] = str_replace(" ","",$file);
                $id = $mVoice->add($data);
            }
            
            $this->redirect(CUri::url("horoscope","updateyear2012","id=".$id."&inform=true"));
        }
        $this->inform = sbInput::get("inform","str");
        $id = sbInput::get("id","int");
        if($id){
            $this->id = $id;
            $this->voice = $mVoice->retrieveByPK("id",$id);
        }

    }

}

?>