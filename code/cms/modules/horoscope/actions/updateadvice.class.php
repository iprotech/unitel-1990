<?php

class Updateadvice extends sbController{

    public function  execute() {
        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mVoice = new MAdvicePeer();
        
        //Edit data
        if(sbInput::get("is_edit","str")){
            $data = array();
            $data['name'] = sbInput::get("name","str");
            $data['status']   = sbInput::get("status","int");
            $data['convert_status']   = 0;
            $data['created_datetime'] = date("Y-m-d H:i:s",time());
            if(sbInput::get("start_date","str")) $startDate = date("Y-m-d",strtotime(sbInput::get("start_date","str")));
            else $startDate = date("Y-m-d",time()-3*24*3600);
            $startDate = date("Y-m-d",time()-3*24*3600);
            $data['start_play'] = $startDate;
            
            if(sbInput::get("end_date","str")) $endDate = date("Y-m-d",strtotime(sbInput::get("end_date","str")));
            else $endDate = date("Y-m-d",time()+3*24*3600);
            $endDate = date("Y-m-d",time()+3*24*3600);
            $data['end_play'] = $endDate;
            
            //Upload file
            $file = CFile::uploadFile("voice_file",sbConfig::get("horoscope_file")."advice/",array("wav","gsm"));
            rename(sbConfig::get("horoscope_file")."advice/".$file,sbConfig::get("horoscope_file")."advice/".str_replace(" ","",$file));
            $id = sbInput::get("id","int");
            
            if($file){
                $orgPath = sbConfig::get("horoscope_file")."advice/".$file;
                $destPath = sbConfig::get("horoscope_mp3_file")."advice/".CFile::removeFileExtension($file).".mp3";
                exec("/usr/bin/ffmpeg -i {$orgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$destPath}");
            }
            if($id){
                if($file){
                    $data['file_path'] = str_replace(" ","",$file);
                }
                $mVoice->update($data,"id=".$id);
                $item = null;
            }else{
                $data['file_path'] = str_replace(" ","",$file);
                $id = $mVoice->add($data);
            }
            
            $this->redirect(CUri::url("horoscope","updateadvice","id=".$id."&inform=true"));
        }
        $this->inform = sbInput::get("inform","str");
        $id = sbInput::get("id","int");
        if($id){
            $this->id = $id;
            $this->voice = $mVoice->retrieveByPK("id",$id);
        }
    }

}

?>