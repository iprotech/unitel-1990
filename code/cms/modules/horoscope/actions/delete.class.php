<?php

class Delete extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }

        $mVoiceDaily = new MVoiceDailyPeer();
        $mVoiceDaily->delete("id=".sbInput::get("id","int",0));
        
        $this->redirect("/index.php/horoscope/daily");
        
    }
}
?>