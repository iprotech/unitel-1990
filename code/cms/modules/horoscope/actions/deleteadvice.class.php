<?php

class Deleteadvice extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }

        $mVoice = new MVoiceAdvicePeer();
        $mVoice->delete("id=".sbInput::get("id","int",0));
        
        $this->redirect("/index.php/horoscope/advice");
        
    }
}
?>