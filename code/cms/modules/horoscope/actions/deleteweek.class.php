<?php

class Deleteweek extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }

        $mVoice = new MWeekPeer();
        $mVoice->delete("id=".sbInput::get("id","int",0));
        
        $this->redirect("/index.php/horoscope/week");
        
    }
}
?>