<?php

class Deleteaugur extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }

        $mVoice = new MAugurPeer();
        $mVoice->delete("id=".sbInput::get("id","int",0));
        
        $this->redirect("/index.php/augur/augur");
        
    }
}
?>