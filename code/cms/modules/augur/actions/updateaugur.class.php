<?php

class Updateaugur extends sbController{

    public function  execute() {
        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mVoice = new MAugurPeer();
        
        //Edit data
        if(sbInput::get("is_edit","str")){
            $data = array();
            $data['number'] = sbInput::get("number","str");
            $data['status']   = sbInput::get("status","int");
            $data['convert_status']   = 0;
            $data['created_datetime'] = date("Y-m-d H:i:s",time());
            
            //Upload file
            $file = CFile::uploadFile("voice_file",sbConfig::get("horoscope_file")."augur/",array("wav","gsm"));
            rename(sbConfig::get("horoscope_file")."augur/".$file,sbConfig::get("horoscope_file")."augur/".str_replace(" ","",$file));
            $id = sbInput::get("id","int");
            
            if($file){
                $orgPath = sbConfig::get("horoscope_file")."augur/".$file;
                $destPath = sbConfig::get("horoscope_mp3_file")."augur/".CFile::removeFileExtension($file).".mp3";
                exec("/usr/bin/ffmpeg -i {$orgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$destPath}");
            }
            if($id){
                if($file){
                    $data['file_path'] = str_replace(" ","",$file);
                }
                $mVoice->update($data,"id=".$id);
                $item = null;
            }else{
                $data['file_path'] = str_replace(" ","",$file);
                $id = $mVoice->add($data);
            }
            
            $this->redirect(CUri::url("augur","updateaugur","id=".$id."&inform=true"));
        }
        $this->inform = sbInput::get("inform","str");
        $id = sbInput::get("id","int");
        if($id){
            $this->id = $id;
            $this->voice = $mVoice->retrieveByPK("id",$id);
        }
    }

}

?>