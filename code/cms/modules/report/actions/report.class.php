<?php

class Report extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }

        $mReport = new MReportPeer();
        $this->msisdn = sbInput::get("msisdn","str");
        $this->startDate = sbInput::get("start_date","str");
        $this->endDate = sbInput::get("end_date","str");
        $this->service = sbInput::get("service","str");


        $this->url = "";
        $condition   = "";

 
        if($this->startDate){
            $date = date("Y-m-d H:i:s",  strtotime($this->startDate));
            $condition .= " AND report.report_date >= '$date'  ";
            $this->url .= "&start_date=".$this->startDate;
        }
        if($this->endDate){
            $date = date("Y-m-d H:i:s",  strtotime($this->endDate));
            $condition .= " AND report.report_date <= '$date'  ";
            $this->url .= "&end_date=".$this->endDate;
        }


        $this->url = CUri::url("report","report")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mReport->getList($condition,$this->page,40);
    }
}
?>