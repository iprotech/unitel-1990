<script>
   $(document).ready(function() {
    $("#end_date").datetimepicker();
    $('#start_date').datetimepicker();
  });
</script>

<div id="content_full" style="margin-top:10px;">
<div id="box">
    <div id="rightnow">
         <h3 class="reallynow">
            <span>
              REPORT & STATISTIC
            </span>
            <br />
        </h3>
    </div>
    <form id="form" action="/index.php/report/report" method="get">
        <style>
            #form_filter{border:none}
            #form_filter td{border:none}
        </style>
        <fieldset id="address">
            <legend>Filter</legend>
            <table style="border:none;width:200px;" id="form_filter">
                <tr style="border:none;">
                    <td><label for="start_date">Start date</label></td>
                    <td><input type="text" id="start_date" name="start_date" tabindex="3" value="<?php echo $startDate; ?>" /></td>
                    <td><label for="end_date">End date</label></td>
                    <td><input type="text" id="end_date" name="end_date" tabindex="4" value="<?php echo $endDate; ?>" /></td>
                </tr>
            </table>
        </fieldset>
        <div align="left">
        <input id="button1" type="submit" value="View data" tabindex="6" />
        <input id="button2" type="Reset" tabindex="7" onclick="window.location='/index.php/report/report'" />
        </div>
    </form>

        <table width="100%">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>New register</th>
                    <th width="150px">Cancel</th>
                    <th width="100px">Real development</th>
                    <th width="100px">Current user</th>
                    <th width="100px">Chargeable</th>
                    <th width="100px">Total revenue</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($datas['items'] as $item): ?>
                <tr>
                    <td><?php echo $item['report_date']; ?> </td>
                    <td>
                            Total: <?php echo $item['new_register']; ?><br/>
                            By sms: <?php echo $item['new_register_sms']; ?><br/>
                            By ivr: <?php echo $item['new_register_ivr']; ?>
                    </td>
                    <td>
                            Total: <?php echo $item['cancel_by_user']+$item['cancel_by_system']; ?><br/>
                            By user: <?php echo $item['cancel_by_user']; ?><br/>
                            By system: <?php echo $item['cancel_by_system']; ?>
                    </td>
                    <td><?php echo $item['real_development']; ?></td>
                    <td><?php echo $item['current_user']; ?></td>
                    <td><?php echo $item['charge_able']; ?></td>
                    <td><?php echo $item['total_revenue']; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
    </table>
    <div id="pager">
        <?php
            if($url=="/index.php/report/report?") $url .= "page=%d";
            else $url .= "&page=%d";
        ?>
        <span style="float:left">Pages &nbsp;</span>
        <?php sbLoader::loadHelper('html'); ?>
        <?php echo CHtml::showPage($datas['totalPage'],$page,$url); ?>
        Total <strong><?php echo $datas['totalItem']; ?></strong> records found
        <div style="clear:both"></div>
    </div>
</div>
</div>