<?php

class Advice extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mAdvice = new MAdvicePeer();
        $this->name = sbInput::get("name","txt");

        $this->url = "";
        $condition   = "";

        if($this->name){
            $condition .= " AND horoscope_advice.name LIKE '%{$this->name}%' ";
            $this->url .= "&name=".$this->name;
        }

        $this->url = CUri::url("horoscope","advice")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mAdvice->getListFile($condition,$this->page,40);
    }
}
?>