<?php

class Dream extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mAugur = new MDreamPeer();
        $this->name = sbInput::get("name","txt");

        $this->url = "";
        $condition   = "";

        if($this->name){
            $condition .= " AND horoscope_augur.name LIKE '%{$this->name}%' ";
            $this->url .= "&name=".$this->name;
        }

        $this->url = CUri::url("horoscope","dream")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mAugur->getListFile($condition,$this->page,40);
    }
}
?>