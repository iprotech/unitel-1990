<?php

class Register extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        sbLoader::loadHelper("file");
        $mMember = new MMemberPeer(); 
        $file = CFile::uploadFile("subscriber",sbConfig::get("subscriber_file"),array("txt","csv"));
        
        if($file){
            $subscribers = file(sbConfig::get("subscriber_file").$file);
            $i=0;
            $sqlInsertFirst = " INSERT INTO member(
                                                                        msisdn,service_id,register_date,
                                                                        start_charging_date,end_charging_date,status,
                                                                        total_money,register_type
                                                   ) VALUES 
                               ";
            $sqlInsert = "";
            foreach($subscribers as $msisdn){
                $msisdn = intval($msisdn);
                if(!$mMember->checkExists($msisdn)){
                    $item['msisdn'] = intval(trim($msisdn));
                    $item['service_id'] = $id;
                    $item['register_date'] = date("Y-m-d H:i:s",time());
                    $item['start_charging_date'] = date("Y-m-d H:i:s",time());
                    $item['end_charging_date'] = date("Y-m-d H:i:s",time());
                    $item['status'] = 1;
                    $item['total_money'] = 0;
                    $item['register_type'] = 1;
                    if($i<100&&$j<(count($subscribers)-1)){
                        $sqlInsert .= ",('{$item['msisdn']}','{$item['service_id']}','{$item['register_date']}','{$item['start_charging_date']}','{$item['end_charging_date']}','{$item['status']}','{$item['total_money']}','{$item['register_type']}')";
                    }else if($i==100||$j==(count($subscribers)-1)){
                        $sqlInsert .= ",('{$item['msisdn']}','{$item['service_id']}','{$item['register_date']}','{$item['start_charging_date']}','{$item['end_charging_date']}','{$item['status']}','{$item['total_money']}','{$item['register_type']}')";
                        $sqlInsert = $sqlInsertFirst.substr($sqlInsert,1).";";
                        $mMember->doInsertMul($sqlInsert);
                        $sqlInsert = "";
                        $i=0;
                    }
                }
                $j++;
                $i++;
                $item = null;
            }
        }
    }
}
?>