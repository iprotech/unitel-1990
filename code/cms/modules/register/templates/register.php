<script>
   $(document).ready(function() {
    date_obj = new Date();
    date_obj_hours = date_obj.getHours();
    date_obj_mins = date_obj.getMinutes();

    if (date_obj_mins < 10) { date_obj_mins = "0" + date_obj_mins; }
    date_obj_time = " '"+date_obj_hours+":"+date_obj_mins+":00'";
    $("#start_time").datepicker({dateFormat: $.datepicker.W3C + date_obj_time});
    $("#end_time").datepicker({dateFormat: $.datepicker.W3C + date_obj_time});
  });
</script>
<div id="content_full">
<div id="box">
    <h3>Auto register</h3>
    <form id="form" action="/index.php/register/register" method="post" enctype="multipart/form-data">
        <input type="hidden" name="is_edit" value="true" />
        <?php if($inform): ?>
        <div style="margin: 10px 40px;color: green;font-weight: bold">Your modification has been updated successfully</div>
        <?php endif; ?>
        <?php if($id): ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?php endif; ?>
        <label for="voice_id">Subscribers</label>
        <input type="file" name="subscriber" />
        <br/>
        <div style="margin: 10px 40px">
            <input id="button1" type="submit" value="Submit" tabindex="6" />
            <input id="button2" type="Reset" tabindex="7" />
        </div>
    </form>
</div>
</div>