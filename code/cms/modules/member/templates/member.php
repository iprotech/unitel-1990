<script>
   $(document).ready(function() {
        $("#start_reg_date").datepicker();
        $("#end_reg_date").datepicker();
        $("#start_cancel_date").datepicker();
        $("#end_cancel_date").datepicker();
  });
</script>


<div id="content_full" style="margin-top: 10px;">
<div id="box">
        <h3>MEMBERS STATISTIC</h3>

    <form id="form" action="/index.php/member/member" method="get">
        <style>
            #form_filter{border:none}
            #form_filter td{border:none}
        </style>
        <fieldset id="address">
            <legend>Filter</legend>
            <table style="border:none;width:200px;" id="form_filter">
                <tr>
                    <td><label for="start_reg_date">Start register date</label></td>
                    <td><input type="text" id="start_reg_date" name="start_reg_date" tabindex="3" value="<?php echo $startRegDate; ?>" /></td>
                    <td><label for="end_reg_date">End register date</label></td>
                    <td><input type="text" id="end_reg_date" name="end_reg_date" tabindex="4" value="<?php echo $endRegDate; ?>" /></td>
                </tr>
                <tr>
                    <td><label for="start_cancel_date">Start cancel date</label></td>
                    <td><input type="text" id="start_cancel_date" name="start_cancel_date" tabindex="3" value="<?php echo $startCancelDate; ?>" /></td>
                    <td><label for="end_date">End cancel date</label></td>
                    <td><input type="text" id="end_cancel_date" name="end_cancel_date" tabindex="4" value="<?php echo $endCancelDate; ?>" /></td>
                </tr>
                <tr>
                    <td style="border:none;"><label for="phone">Phone number</label></td>
                    <td><input name="phone" id="phone" type="text" value="<?php echo $phoneNumber; ?>" tabindex="1" /></td>
                    <td><label for="status">Member status</label></td>
                    <td>
                        <?php $memberStatus = sbConfig::get("member_status"); ?>
                        <select id="status" name="status">
                            <option></option>
                            <?php foreach($memberStatus as $key=>$value): ?>
                            <option value="<?php echo $key; ?>" <?php if($status==$key) echo "selected"; ?>><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            </table>
        </fieldset>
		
		<fieldset id="address">
            <legend>Member statistic </legend>
            <table>
                 <tr>
                     <td>Total member: <?php echo $datas['totalItem']; ?></td>
                 </tr>
            </table>
        </fieldset>

        <div align="left">
        <input id="button1" type="submit" value="View data" tabindex="6" />
        <input id="button1" type="submit" value="Export" name="Export" tabindex="7" />
        <input id="button2" type="Reset" tabindex="8" />
        </div>
    </form>
    <table width="100%">
        <thead>
            <tr>
                <th width="50px">Member ID</th>
                <th width="100px">Phone number</th>
                <th width="100px">Register date</th>
                <th width="100px">Expried date</th>
                <th width="100px">Status</th>
                <th width="50px">Total money</th>
                <th width="70px">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php $memberStatus = sbConfig::get("member_status"); ?>
            <?php foreach(@$datas['items'] as $item): ?>
            <tr>
                <td><?php echo $item['id']; ?></td>
                <td><?php echo $item['msisdn']; ?></td>
                <td><?php echo date("d/m/Y H:i:s",  strtotime($item['register_date'])); ?></td>
                <td><?php if($item['end_charging_date']) echo date("d/m/Y H:i:s",  strtotime($item['end_charging_date'])); ?></td>
                <td>
                    <?php echo $memberStatus[$item['status']]; ?>
                    <p>
                    <?php if($item['status']>2): ?>
                    <b><?php echo date("d/m/Y H:i:s",strtotime($item['cancel_date'])); ?></b>
                    <?php endif; ?>
                    </p>
                </td>
                <td>
                    <?php echo $item['total_money']; ?>
                </td>
                <td>
                    <p><a href="<?php echo CUri::url("member","charging","member_id=".$item['id']); ?>">View Charging</a></p>
                    <p><a href="<?php echo CUri::url("member","cancel","member_id=".$item['id']); ?>" onclick="return confirm('Are you sure cancel this member?')">Cancel</a></p>
                    <p><a href="<?php echo CUri::url("member","active","member_id=".$item['id']); ?>" onclick="return confirm('Are you sure active this member?')">Active</a></p>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div id="pager">
        <?php
            if($url=="/index.php/sms/mo?") $url .= "page=%d";
            else $url .= "&page=%d";
        ?>
        <span style="float:left">Pages &nbsp;</span>
        <?php sbLoader::loadHelper('html'); ?>
        <?php echo CHtml::showPage($datas['totalPage'],$page,$url); ?>
        Total <strong><?php echo $datas['totalItem']; ?></strong> records found
        <div style="clear:both"></div>
    </div>
</div>
</div>