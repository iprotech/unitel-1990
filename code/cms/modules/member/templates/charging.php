<script>
   $(document).ready(function() {
        $("#start_reg_date").datepicker();
        $("#end_reg_date").datepicker();
        $("#start_cancel_date").datepicker();
        $("#end_cancel_date").datepicker();
  });
</script>


<div id="content_full" style="margin-top:10px">
<div id="box">
   <h3>MEMBERS CHARING STATISTIC</h3>     
   <table width="100%">
        <thead>
            <tr>
                <th width="50px">Member ID</th>
                <th width="100px">Phone number</th>
                <th width="100px">Charging date</th>
                <th width="100px">End date</th>
                <th width="50px">Total money</th>
                <th width="70px">Service ID</th>
                <th width="70px">MO ID</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach(@$datas as $item): ?>
            <tr>
                <td><?php echo $item['member_id']; ?></td>
                <td><?php echo $item['msisdn']; ?></td>
                <td><?php echo date("d/m/Y H:i:s",  strtotime($item['start_charging_date'])); ?></td>
                <td><?php echo date("d/m/Y H:i:s",  strtotime($item['end_charging_date'])); ?></td>
                <td>
                    <?php echo $item['total_money']; ?>
                </td>
                <td><?php echo $item['service_name'] ?></td>
                <td>
                    <p><a href="<?php echo CUri::url("sms","mo","id=".$item['mo_id']); ?>">View MO</a></p>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div id="pager">
        <?php
            if($url=="/index.php/sms/mo?") $url .= "page=%d";
            else $url .= "&page=%d";
        ?>
        <span style="float:left">Pages &nbsp;</span>
        <?php sbLoader::loadHelper('html'); ?>
        <?php echo CHtml::showPage($datas['totalPage'],$page,$url); ?>
        Total <strong><?php echo $datas['totalItem']; ?></strong> records found
        <div style="clear:both"></div>
    </div>
</div>
</div>