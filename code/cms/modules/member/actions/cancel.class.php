<?php
class Cancel extends sbController{

    public function execute(){

        if($this->getUser()->getAttribute("group_id")!=1){
            $this->redirect("/index.php/account/login");
            exit();
        }
        $mMember = new MMemberPeer();
        $id = sbInput::get("member_id","int");
        $status = sbConfig::get("member_status");
        $mMember->update(array("status"=>5,"cancel_date"=>date("Y-m-d H:i:s",time())),"id=".$id);
        echo "<script language='javascript'>alert('Update member successfully');</script>";
        $this->redirect("/index.php/member");
    }
}

?>