<?php
class Active extends sbController{

    public function execute(){

        if($this->getUser()->getAttribute("group_id")!=1){
            $this->redirect("/index.php/account/login");
            exit();
        }
        $mMember = new MMemberPeer();
        $id = sbInput::get("member_id","int");
        $mMember->update(array("status"=>"1"),"id=".$id);
        echo "<script language='javascript'>alert('Update member successfully');</script>";
        $this->redirect("/index.php/member");
    }
}

?>