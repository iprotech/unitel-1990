<?php
class Charging extends sbController{

    public function execute(){

         if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }
        $mCharing = new MChargingHistoryPeer();
        $this->memberId = sbInput::get("member_id","int");
        $this->url = "/index.php/member/charging?member_id=".$this->memberId;
        if($this->memberId){
            $condition = " AND member_id=".$this->memberId;
            $this->datas = $mCharing->getAllCharging($condition);
        }
    }
}

?>
