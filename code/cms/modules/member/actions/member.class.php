<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Member extends sbController{

    public function execute(){

        
        $mMember = new MMemberPeer();
        $mService = new MServicePeer();
        $this->services = $mService->getAllServices();
        $this->phoneNumber = sbInput::get("phone","txt");
        $this->serviceId   = sbInput::get("service_id","int");
        $this->startRegDate   = sbInput::get("start_reg_date","txt");
        $this->endRegDate     = sbInput::get("end_reg_date","txt");
        $this->startCancelDate   = sbInput::get("start_cancel_date","txt");
        $this->endCancelDate     = sbInput::get("end_cancel_date","txt");
        $this->status = sbInput::get("status","int");
       
        $this->url = "";
        $condition   = "";
        if($this->phoneNumber){
            $condition .= " AND msisdn LIKE '%{$this->phoneNumber}%' ";
            $this->url .= "&phone=".$this->phoneNumber;
        }
        if($this->startRegDate){
            $condition .= " AND date(register_date) >= '".date('Y-m-d',strtotime($this->startRegDate))."'";
            $this->url .= "&start_reg_date=".$this->startDate;
        }
        if($this->endRegDate){
            $condition .= " AND  date(register_date) <= '".date('Y-m-d',strtotime($this->endRegDate))."'";
            $this->url .= "&end_reg_date=".$this->endRegDate;
        }
        if($this->serviceId){
            $condition .= " AND service_id=".$this->serviceId;
            $this->url .= "&service_id=".$this->serviceId;
        }
        if($this->startCancelDate){
            $condition .= " AND date(cancel_date) >= '".date('Y-m-d',strtotime($this->startCancelDate))."'";
            $this->url .= "&start_cancel_date=".$this->startCancelDate;
        }
        if($this->endCancelDate){
            $condition .= " AND date(cancel_date) <= '".date('Y-m-d',strtotime($this->endCancelDate))."'";
            $this->url .= "&end_cancel_date=".$this->endCancelDate;
        }

        if($this->status){
            $condition .= " AND member.status=".$this->status;
            $this->url .= "&status=".$this->status;
        }

        //echo $condition;
        $this->url = "/index.php/member/member?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        if(sbInput::get("Export","str")){
            $datas = $mMember->getListMember($condition,$this->page,100000);
            $excel = "<table><tr><td>Phonenumber</td><td>Register date</td><td>Cancel date</td><td>Total charge money</td></tr>";
            foreach($datas['items'] as $member){
                if($member['status']>2){
                    $cancelDate = date("d/m/Y H:i:s",strtotime($member['cancel_date']));
                }else{
                    $cancelDate = "";
                }
                $excel .= "<tr><td>{$member['msisdn']}</td>
                               <td>".date("d/m/Y H:i:s",strtotime($member['register_date']))."</td>
                               <td>{$cancelDate}</td>
                               <td>{$member['total_money']}</td>
                          </tr>";
            }
            $excel .= "</table>";
            //echo $excel;
            file_put_contents("/var/www/html/data/member/member.xls",$excel);
            $this->redirect("/data/member/member.xls");
        }
        
        $this->datas = $mMember->getListMember($condition,$this->page,40);
    }

}
?>
