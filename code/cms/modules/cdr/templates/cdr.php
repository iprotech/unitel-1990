<script>
   $(document).ready(function() {
    $("#end_date").datetimepicker();
    $('#start_date').datetimepicker();
  });
</script>

<div id="content_full" style="margin-top:10px;">
<div id="box">
    <div id="rightnow">
         <h3 class="reallynow">
            <span>
              CDR & STATISTIC
            </span>
            <br />
        </h3>
    </div>
    <form id="form" action="/index.php/cdr/cdr" method="get">
        <style>
            #form_filter{border:none}
            #form_filter td{border:none}
        </style>
        <fieldset id="address">
            <legend>Filter</legend>
            <table style="border:none;width:200px;" id="form_filter">
                <tr style="border:none;">
                    <td style="border:none;"><label for="phone">Phonenumber</label></td>
                    <td><input name="msisdn" id="phone" type="text" value="<?php echo $msisdn; ?>" tabindex="1" /></td>
                    <td><label for="start_date">Start date</label></td>
                    <td><input type="text" id="start_date" name="start_date" tabindex="3" value="<?php echo $startDate; ?>" /></td>
                    <td><label for="end_date">End date</label></td>
                    <td><input type="text" id="end_date" name="end_date" tabindex="4" value="<?php echo $endDate; ?>" /></td>
                </tr>
                <tr>
                    <td>Service</td>
                    <td>
                        <select name="service" id="service" style="width:150px;">
                            <option></option>
                            <?php foreach($services as $key=>$value): ?>
                             <option value="<?php echo $key; ?>" <?php if($key==$service) echo "selected"; ?>><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset id="address">
            <legend>Total Block 30s</legend>
            <?php echo $datas['totalBlock']; ?>
        </fieldset>


        <div align="left">
        <input id="button1" type="submit" value="View data" tabindex="6" />
        <input id="button2" type="Reset" tabindex="7" onclick="window.location='/index.php/result/result'" />
        </div>
    </form>

        <table width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Msisdn</th>
                    <th width="150px">Call time</th>
                    <th width="100px">Total duration</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($datas['items'] as $item): ?>
                <tr>
                    <td><?php echo $item['id']; ?> </td>
                    <td><?php echo $item['src']; ?></td>
                    <td><?php echo date("d/m/Y H:i:s",  strtotime($item['start'])); ?></td>
                    <td><?php
                           echo  $item['duration'];
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
    </table>
    <div id="pager">
        <?php
            if($url=="/index.php/schedule/schedule?") $url .= "page=%d";
            else $url .= "&page=%d";
        ?>
        <span style="float:left">Pages &nbsp;</span>
        <?php sbLoader::loadHelper('html'); ?>
        <?php echo CHtml::showPage($datas['totalPage'],$page,$url); ?>
        Total <strong><?php echo $datas['totalItem']; ?></strong> records found
        <div style="clear:both"></div>
    </div>
</div>
</div>