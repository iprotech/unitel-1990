<?php

class Cdr extends sbController{

    public function execute(){

        if(!$this->getUser()->getAuthenticated()){
            $this->redirect("/index.php/account/login");
            exit();
        }

        $mCdr = new MCdrPeer();
        $this->msisdn = sbInput::get("msisdn","str");
        $this->startDate = sbInput::get("start_date","str");
        $this->endDate = sbInput::get("end_date","str");
        $this->service = sbInput::get("service","str");


        $this->url = "";
        $condition   = "";

        if($this->msisdn){
            $condition .= " AND cdr.src LIKE '%{$this->msisdn}%' ";
            $this->url .= "&msisdn=".$this->msisdn;
        }
        if($this->startDate){
            $date = date("Y-m-d H:i:s",  strtotime($this->startDate));
            $condition .= " AND cdr.start >= '$date'  ";
            $this->url .= "&start_date=".$this->startDate;
        }
        if($this->endDate){
            $date = date("Y-m-d H:i:s",  strtotime($this->endDate));
            $condition .= " AND cdr.start <= '$date'  ";
            $this->url .= "&end_date=".$this->endDate;
        }
        if($this->service){
            $condition .= " AND accountcode LIKE '%{$this->service}%' ";
            $this->url .= "&service=".$this->service;
        }

        $condition .= " AND duration<5000 ";

        $this->url = CUri::url("cdr","cdr")."?".substr($this->url,1);
        $this->page = sbInput::get("page","int");
        $this->datas = $mCdr->getListCdr($condition,$this->page,40);
        $this->services = sbConfig::get("services");
    }
}
?>