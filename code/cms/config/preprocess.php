<?php

$classess = array(
                    "lib/kernel/controller/",
                    "lib/kernel/model/",
                    "lib/kernel/helper/",
                    "lib/kernel/portal/",
                    "lib/kernel/routing/",
                    "lib/kernel/config/",
                    "lib/kernel/view/",
                    "lib/kernel/user/",
                    "lib/model/"
                );
function __autoload($className){
    global $classess;
    foreach($classess as $class){
       if(file_exists($class.$className.".class.php")){
           require_once($class.$className.".class.php");
           break;
       }
    }
    require_once("lib/kernel/helper/I18NHelper.php");
}
?>
