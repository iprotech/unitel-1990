<?php

session_start();
require_once 'settings.php';
require_once 'preprocess.php';

sbConfig::add(
    array(
        "metaKeyword" => "sbphp.vn",
        "metaDescription" => "sbphp.vn framework"
    )
);

sbConfig::add(
      array(
          "days"=>array(
              "1"=>"Monday",
              "2"=>"Tuesday",
              "3"=>"Wenesday",
              "4"=>"Thursday",
              "5"=>"Friday",
              "6"=>"Saturday",
              "7"=>"Sunday",
          ),
      )
);

sbConfig::add(
    array(
        "group" =>   array(
                            "1"=>array("8090","0"),
                            "2"=>array("8190","500"),
                            "3"=>array("8290","1000"),
                            "4"=>array("8390","2000"),
                            "5"=>array("8490","3000"),
                            "6"=>array("8590","4000"),
                            "7"=>array("8690","5000"),
                            "8"=>array("8790","10000"),
                            "9"=>array("8890","15000"),
                        )
        )
);

sbConfig::add(
    array(
        "member_status" => array("1"=>"Active","2"=>"Suspend","3"=>"Cancel","4"=>"Cancel By System","5"=>"Cancel by admin")
    )
);

sbConfig::add(
    array(
        "mo_status" => array("0"=>"Pending","1"=>"Complete","2"=>"Error")
    )
);

sbConfig::add(
    array(
        "mt_status" => array("0"=>"Pending","1"=>"Complete","2"=>"Error")
    )
);

sbConfig::add(
    array(
        "command_type" => array("1"=>"ON","2"=>"OFF","3"=>"REMAIN","4"=>"RFREE","5"=>"HELP")
    )
);

sbConfig::add(
    array(
        "content_status" => array("1"=>"Processing","2"=>"Complete")
    )
);

sbConfig::add(
    array(
        "voice_common"=>"data/origin/voice_common/","horoscope_file"=>"data/origin/","voice_common_mono"=>"data/mono/voice_common/",
        "subscriber_file"=>"data/subscriber_file/","horoscope_mp3_file"=>"data/mp3/",
        "voice_common_new"=>"data/origin/voice_common_new/",
        "voice_common_mono_new"=>"data/mono/voice_common_new/"
    )
);

sbConfig::add(
            array(
                    "services"=>array(
                                        "horoscope"=>"Horoscope"
                                     )
                )
        );
sbConfig::add(
        array("voice_horoscope_common"=>"data/horoscope/common/")
)

?>