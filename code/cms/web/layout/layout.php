<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HOROSCOPE CMS MANAGEMENT</title>
<link rel="stylesheet" type="text/css" href="/web/css/theme.css" />
<link rel="stylesheet" type="text/css" href="/web/css/style.css" />
<link rel="stylesheet" type="text/css" href="/web/css/jquery-ui.css" />
<script src="/web/js/jquery.1.4.2.min.js" type="text/javascript"></script>
<script src="/web/js/jquery.ui.core.js" type="text/javascript"></script>
<script src="/web/js/jquery.ui.datepicker.js" type="text/javascript"></script>
<script src="/web/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script type="text/javascript" src="/jwplayer/jwplayer.js"></script>
<script type="text/javascript">jwplayer.key="ABCDEFGHIJKLMOPQ";</script>

<script>
   var StyleFile = "theme" + document.cookie.charAt(6) + ".css";
   document.writeln('<link rel="stylesheet" type="text/css" href="/web/css/' + StyleFile + '">');
</script>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="css/ie-sucks.css" />
<![endif]-->
</head>
<body>
    <div type="text" id="datepicker"></div>
    <div id="container">
        <?php include("header.php"); ?>
        <div id="wrapper">
                <?php echo $portal->getContent(); ?>
        </div>
        <?php include("footer.php"); ?>
    </div>
</body>
</html>