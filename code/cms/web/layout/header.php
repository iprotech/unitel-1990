<div id="header">
    <div id="topInfo">
        <div class="fl"><h2> UNITEL HOROSCOPE CMS MANAGEMENT</h2></div>
        <div id="user">
            <?php
                $username = sbUser::getInstance()->getAttribute("username");
                $id       = sbUser::getInstance()->getAttribute("id");
            ?>
            You logged as <a href="/index.php/account/update?id=<?php echo $id; ?>"> <?php echo $username ?>! </a> | <a href="/index.php/account/update?id=<?php echo $id; ?>" style="color:#fba335">Your account</a> | <a href="/index.php/account/logout">Logout</a>
        </div>
        <div class="cl">&nbsp;</div>
    </div>
    <div id="topmenu">
        <?php
            $aRouting = sbRouting::parse();
            $scheduleStatus = $subscriberStatus = false;
            switch($aRouting['module']){
                case "horoscope":
                    $horoscope = "current";
                    switch($aRouting['action']){
                        case 'daily':
                        case 'updatedaily':
                            $dailyStatus = "sub_active";
                            break;
                        case 'advice':
                        case 'updateadvice':
                            $adviceStatus = "sub_active";
                            break;
                        case 'augur':
                        case 'updateaugur':
                            $augurStatus = "sub_active";
                            break;
                        case 'dream':
                        case 'updatedream':
                            $dreamStatus = "sub_active";
                            break;
                        case 'week':
                        case 'updateweek':
                            $weekStatus = "sub_active";
                            break;
                        case 'month':
                        case 'updatemonth':
                            $monthStatus = "sub_active";
                            break;
                        case 'year2012':
                        case 'updateyear2012':
                            $year2012Status = "sub_active";
                            break;
                        case 'config':
                            $configStatus = "sub_active";
                            break;
                        case 'character':
                            $characterStatus = "sub_active";
                            break;
                        case 'machine':
                            $machineStatus = "sub_active";
                            break;
                    }
                    break;
                case "dating":
                    $dating = "current";
                    switch($aRouting['action']){
                        case 'member':
                        case 'cancel':
                            $member = "sub_active";
                            break;
                        case 'config':
                            $configStatus = "sub_active";
                            break;
                    }
                    break;
                case "schedule":
                    $scheduleStatus = "current";
                    switch($aRouting['action']){
                        case 'schedule':
                        case 'update':
                            $scheduleSub = "sub_active";
                            break;
                        case 'content':
                        case 'updatecontent':
                            $scheduleContent = "sub_active";
                            break;
                    }
                    break;
                case 'voting':
                    $voting="current";
                    switch($aRouting['action']){
                        case 'voting':
                        case 'update':
                            $prizeStatus = "sub_active";
                            break;
                        case 'config':
                            $votingConfig = "sub_active";
                            break;
                    }
                    break;
                case "advice":
                    $horoscopeAdvice = "current";
                    break;
                case "augur":
                    $horoscopeAugur = "current";
                    break;
                case "dream":
                    $horoscopeDream = "current";
                    break;
                case "statistic":
                    $statistic = "current";
                    break;
                case "account":
                    $accountStatus = "current";
                    break;
                case "member":
                    $member = "current";
                    break;
                case "cdr":
                    $cdr = "current";
                    break;
                 case "report":
                    $report = "current";
                    break;
                case "cregister":
                    $register = "current";
                    break;
                case "config":
                    $config = "current";
                    break;
                case "confignw":
                    $confignw = "current";
                    break;
                default:
                    $dashboad = "current";
            }
        ?>
        <ul>
            <?php if(sbUser::getInstance()->getAttribute("group_id")==1): ?>
            <li class="<?php echo $config; ?>"><a href="/index.php/config/config">CONFIG</a></li>
            <li class="<?php echo $confignw; ?>"><a href="/index.php/confignw/confignw">CONFIGNEW</a></li>
            <li class="<?php echo $horoscope; ?>"><a href="/index.php/horoscope/daily">HOROSCOPE</a></li>
            <li class="<?php echo $horoscopeAdvice; ?>"><a href="/index.php/advice/advice">ADVICE</a></li>
            <li class="<?php echo $horoscopeAugur; ?>"><a href="/index.php/augur/augur">AUGUR</a></li>
            <li class="<?php echo $horoscopeDream; ?>"><a href="/index.php/dream/dream">DREAM</a></li>
            <?php endif; ?>
            <!--
            <li class="<?php echo $scheduleStatus; ?>"><a href="/index.php/schedule/schedule">SCHEDULE</a></li>
            <li class="<?php echo $subscriberStatus; ?>"><a href="/index.php/subscriber">SUBSCRIBER</a></li>
            -->
            <li class="<?php echo $cdr; ?>"><a href="/index.php/cdr">CDR STATISTIC</a></li>
            <li class="<?php echo $member; ?>"><a href="/index.php/member/member">MEMBERS</a></li>
            <li class="<?php echo $statistic; ?>"><a href="/index.php/statistic/statistic">CHARGING</a></li>
            <li class="<?php echo $report; ?>"><a href="/index.php/report/report">REPORT</a></li>
            <?php if(sbUser::getInstance()->getAttribute("group_id")==1): ?>
            <li class="<?php echo $accountStatus; ?>"><a href="/index.php/account">USERS</a></li>
            <?php endif; ?>
        </ul>
  </div>
</div>
<?php if($aRouting['module']=="schedule"): ?>
<div id="top-panel">
    <div id="panel">
        <ul>
            <li><a href="/index.php/schedule/schedule" class="report <?php echo $scheduleSub; ?>">SCHEDULE</a></li>
            <li><a href="/index.php/schedule/content" class="promotions <?php echo $scheduleContent; ?>">VOICE CONTENT</a></li>
        </ul>
    </div>
</div>
<?php endif; ?>
<?php if($aRouting['module']=="subscriber"): ?>
<div id="top-panel">
    <div id="panel">
        <ul>
            <li><a href="/index.php/subscriber/subscriber" class="modules <?php echo $servStatus; ?>">SUBSCRIBER</a></li>
        </ul>
    </div>
</div>
<?php endif; ?>

<?php if($aRouting['module']=="horoscope"): ?>
<div id="top-panel">
    <div id="panel">
        <ul>
            <li><a href="/index.php/horoscope/daily" class="modules <?php echo $dailyStatus; ?>">HOROSCOPE DAILY</a></li>
            <li><a href="/index.php/horoscope/week" class="promotions <?php echo $weekStatus; ?>">HOROSCOPE WEEK</a></li>
            <li><a href="/index.php/horoscope/month" class="promotions <?php echo $monthStatus; ?>">HOROSCOPE MONTH</a></li>
            <li><a href="/index.php/horoscope/year2012" class="promotions <?php echo $year2012Status; ?>">HOROSCOPE YEAR</a></li>
        </ul>
    </div>
</div>
<?php endif; ?>