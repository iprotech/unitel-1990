<?php
//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");

try{
    $conn = new sbMysqlPDO($server, $user, $password, $db);
}catch(Exception $e){
    file_put_contents($realPath."log/{$mod}.txt","\nError when connect to database server|".date("Y-m-d H:i:s",time()));
    sleep(5);
}
$_content_file = "/var/www/html/data/origin/";
$_content_mono_file = "/var/www/html/data/mono/";
$_content_mp3_file = "/var/www/html/data/mp3/";

$sqlSelectQuestion = " SELECT * FROM horoscope_daily WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
$questions = $conn->doSelect($sqlSelectQuestion);
foreach($questions as $question){
    $fileOrgPath = $_content_file."daily/".$question['file_path'];
    $fileDestPath = $_content_mono_file."daily/".$question['file_path'];
    //Convert file
    exec("/usr/bin/ffmpeg  -y -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac  1 $fileDestPath");
    
    //Rename
    rename($_content_mono_file."daily/".$question['file_path'],$_content_mono_file."daily/".getFileName($question['file_path']).".alaw");
    
    $fileDestPathMp3 = $_content_mp3_file."daily/".getFileName($question['file_path']).".mp3";
    exec("/usr/bin/ffmpeg -i {$fileOrgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$fileDestPathMp3}");
    
    $sqlUpdate = " UPDATE horoscope_daily SET convert_status=1 WHERE id='{$question['id']}' ";
    $conn->doUpdate($sqlUpdate);

    sleep(1);
}
sleep(1);


$sqlSelectQuestion = " SELECT * FROM horoscope_week WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
$questions = $conn->doSelect($sqlSelectQuestion);
foreach($questions as $question){
    $fileOrgPath = $_content_file."/week/".$question['file_path'];
    $fileDestPath = $_content_mono_file."/week/".$question['file_path'];
    //Convert file
    exec("/usr/bin/ffmpeg  -y -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
    //Rename
    rename($_content_mono_file."/week/".$question['file_path'],$_content_mono_file."/week/".getFileName($question['file_path']).".alaw");
    
    $fileDestPathMp3 = $_content_mp3_file."week/".getFileName($question['file_path']).".mp3";
    exec("/usr/bin/ffmpeg -i {$fileOrgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$fileDestPathMp3}");
    
    $sqlUpdate = " UPDATE horoscope_week SET convert_status=1 WHERE id='{$question['id']}' ";
    $conn->doUpdate($sqlUpdate);

    sleep(1);
}
sleep(1);

$sqlSelectQuestion = " SELECT * FROM horoscope_month WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
$questions = $conn->doSelect($sqlSelectQuestion);
foreach($questions as $question){
    $fileOrgPath = $_content_file."/month/".$question['file_path'];
    $fileDestPath = $_content_mono_file."/month/".$question['file_path'];
    //Convert file
    exec("/usr/bin/ffmpeg  -y -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
    //Rename
    rename($_content_mono_file."/month/".$question['file_path'],$_content_mono_file."/month/".getFileName($question['file_path']).".alaw");
    
    
    $fileDestPathMp3 = $_content_mp3_file."month/".getFileName($question['file_path']).".mp3";
    exec("/usr/bin/ffmpeg -i {$fileOrgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$fileDestPathMp3}");
    
    $sqlUpdate = " UPDATE horoscope_month SET convert_status=1 WHERE id='{$question['id']}' ";
    $conn->doUpdate($sqlUpdate);

    sleep(1);
}
sleep(1);


$sqlSelectQuestion = " SELECT * FROM horoscope_year2012 WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
$questions = $conn->doSelect($sqlSelectQuestion);
foreach($questions as $question){
    $fileOrgPath = $_content_file."/year/".$question['file_path'];
    $fileDestPath = $_content_mono_file."/year/".$question['file_path'];
    //Convert file
    exec("/usr/bin/ffmpeg -y -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
    //Rename
    rename($_content_mono_file."/year/".$question['file_path'],$_content_mono_file."/year/".getFileName($question['file_path']).".alaw");
    
    $fileDestPathMp3 = $_content_mp3_file."year/".getFileName($question['file_path']).".mp3";
    exec("/usr/bin/ffmpeg -i {$fileOrgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$fileDestPathMp3}");
    
    $sqlUpdate = " UPDATE horoscope_year2012 SET convert_status=1 WHERE id='{$question['id']}' ";
    $conn->doUpdate($sqlUpdate);

    sleep(1);
}
sleep(1);


$sqlSelectQuestion = " SELECT * FROM horoscope_advice WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
$questions = $conn->doSelect($sqlSelectQuestion);
foreach($questions as $question){
    $fileOrgPath = $_content_file."/advice/".$question['file_path'];
    $fileDestPath = $_content_mono_file."/advice/".$question['file_path'];
    //Convert file
    exec("/usr/bin/ffmpeg -y -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
    //Rename
    rename($_content_mono_file."/advice/".$question['file_path'],$_content_mono_file."/advice/".getFileName($question['file_path']).".alaw");
    
    $fileDestPathMp3 = $_content_mp3_file."advice/".getFileName($question['file_path']).".mp3";
    exec("/usr/bin/ffmpeg -i {$fileOrgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$fileDestPathMp3}");
    
    $sqlUpdate = " UPDATE horoscope_advice SET convert_status=1 WHERE id='{$question['id']}' ";
    $conn->doUpdate($sqlUpdate);

    sleep(1);
}
sleep(1);


$sqlSelectQuestion = " SELECT * FROM horoscope_augur WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
$questions = $conn->doSelect($sqlSelectQuestion);
foreach($questions as $question){
    $fileOrgPath = $_content_file."/augur/".$question['file_path'];
    $fileDestPath = $_content_mono_file."/augur/".$question['file_path'];
    //Convert file
    exec("/usr/bin/ffmpeg -y -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
    //Rename
    rename($_content_mono_file."/augur/".$question['file_path'],$_content_mono_file."/augur/".getFileName($question['file_path']).".alaw");
    
    $fileDestPathMp3 = $_content_mp3_file."augur/".getFileName($question['file_path']).".mp3";
    exec("/usr/bin/ffmpeg -i {$fileOrgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$fileDestPathMp3}");
    
    $sqlUpdate = " UPDATE horoscope_augur SET convert_status=1 WHERE id='{$question['id']}' ";
    $conn->doUpdate($sqlUpdate);

    sleep(1);
}


$sqlSelectQuestion = " SELECT * FROM horoscope_dream WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
$questions = $conn->doSelect($sqlSelectQuestion);
foreach($questions as $question){
    $fileOrgPath = $_content_file."/dream/".$question['file_path'];
    $fileDestPath = $_content_mono_file."/dream/".$question['file_path'];
    //Convert file
    exec("/usr/bin/ffmpeg -y -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
    //Rename
    rename($_content_mono_file."/dream/".$question['file_path'],$_content_mono_file."/dream/".getFileName($question['file_path']).".alaw");
    
    $fileDestPathMp3 = $_content_mp3_file."dream/".getFileName($question['file_path']).".mp3";
    exec("/usr/bin/ffmpeg -i {$fileOrgPath} -y -vn -ar 44100 -ac 2 -ab 192 -f mp3 {$fileDestPathMp3}");
    
    $sqlUpdate = " UPDATE horoscope_dream SET convert_status=1 WHERE id='{$question['id']}' ";
    $conn->doUpdate($sqlUpdate);

    sleep(1);
}

sleep(1);


?>
