<?php
session_start();  

//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
require_once('mailer/class.phpmailer.php');
require_once('mailer/class.smtp.php');


$_msisdn = "2098868585";
$_email_account = "sinv@iprotech.vn"; 
$_email_password = "sinv@123";
//$_mail_list = array("thien@iprotech.vn","sivv@iprotech.vn");
$_mail_list = array(1=>"iprovasmonitor@gmail.com",2=>"it48thwru@gmail.com");
$errorMsg = array();
$totalCPU='';
$totalRamUse='';

//MYSQL
$conn = new sbMysqlPDO($server, $user, $password, $db);
if(!$conn){
    $errorMsg[] = "Can not connect to MYSQL ";
}
	//HDD
    $hdd = explode("\n",shell_exec('df -h'));
    $hdd = explode(" ",preg_replace('/\s+/', ' ', $hdd[2]));
    if(intval($hdd[4])>80){
        $errorMsg[]= "HDD is full: {$hdd[4]}, available {$hdd[3]} ";
    }

    //CPU
    $cpus = explode("\n",shell_exec('top -b -n3 | grep "Cpu(s)"'));
    var_dump($cpus);
    for($i=0;$i<(count($cpus)-1);$i++){
        $arCpu = explode(" ",preg_replace('/\s+/', ' ',$cpus[$i]));
        $totalCPU += intval($arCpu[1])+intval($arCpu[2]);
    }
    $totalCPU = intval($totalCPU/3);
    if($totalCPU>70){
        $errorMsg[]= "CPU is hight: ".$totalCPU;
    }
    
    //RAM
    $rams = explode("\n",shell_exec('top -b -n3 | grep "Mem"'));
    var_dump($rams);
    for($i=0;$i<(count($rams)-1);$i++){
        $arRam = explode(" ",preg_replace('/\s+/', ' ',$rams[$i]));
        $totalRamUse += intval($arRam[3])/intval($arRam[1]);
    }
    $totalRamUse = intval($totalRamUse/3);
    if($totalRamUse>70){
        $errorMsg[]= "RAM is hight: ".$totalRamUse;
    }	
	// Canh bao Ping MSC
	$ping1 = shell_exec("ping -c 1 10.20.3.1 | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }'");
	if($ping1 == 0){
		echo "Can not ping to MSC 10.20.3.1";
	}
	
	$ping1 = shell_exec("ping -c 1 10.20.3.17 | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }'");
	if($ping1 == 0){
		echo "Can not ping to MSC 10.20.3.17";
	}
		
	$ping1 = shell_exec("ping -c 1 10.20.5.1 | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }'");
	if($ping1 == 0){
		echo "Can not ping to MSC 10.20.5.1";
	}
	
	$ping1 = shell_exec("ping -c 1 10.20.5.17 | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }'");
	if($ping1 == 0){
		echo "Can not ping to MSC 10.20.5.17";
	}
	
	// Canh bao SMSC
	$ping1 = shell_exec("ping -c 1 10.78.7.165 | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }'");
	if($ping1 == 0){
		echo "Can not ping to SMSC 10.78.7.165";
	}
	
	// CHARING GW
	$ping1 = shell_exec("ping -c 1 10.78.17.17 | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }'");
	if($ping1 == 0){
		echo "Can not ping to CHARING GW 10.78.17.17";
	}
	
	// Canh bao SERVICE MYSQLD
	$mysqld = shell_exec("service mysqld status | grep 'running'");
	if(empty($mysqld)){
		echo "Service mysqld Stop";
	}
	
	// Canh bao SERVICE ASTERISK
	$asterisk = shell_exec("service asterisk status | grep 'running'");
	if(empty($asterisk)){
		echo "Service asterisk Stop";
	}
	
	// Canh bao SERVICE HTTPD
	$httpd = shell_exec("service httpd status | grep 'running'");
	if(empty($httpd)){
		echo "Service httpd Stop";
	}
	
	// Canh bao SERVICE KANNEL
	$kannel = shell_exec("service kannel status | grep 'running'");
	if(empty($kannel)){
		echo "Service kannel Stop";
	}

// Check Info
$info = viewInfo($_msisdn);
if($info[3] != $_msisdn){
    $errorMsg[] = "Can not get phone number information (viewInfo) {$info[1]} ";
}

//Check charging
echo $sqlCheckToday = " SELECT sum(total_money) as total FROM charging_history WHERE start_charging_date >date(now()) ";
$rToday = $conn->doSelectOne($sqlCheckToday);

if(isset($_SESSION['r']) && $_SESSION['r'] == $rToday['total']){
    $errorMsg[] = 'Please checking now. Have problem on charging process';
    session_unset($_SESSION['r']);
}

$timeCheck = date("Y-m-d H:i:s",time()-24*3600);
echo $sqlCheckYesterday = " SELECT sum(total_money) as total FROM charging_history WHERE start_charging_date>=date('$timeCheck') AND start_charging_date<='$timeCheck' ";
$rYesterday = $conn->doSelectOne($sqlCheckYesterday);

if($rToday['total']*2<$rYesterday['total']){
    $errorMsg[] = "Have problem on charging process, today: {$rToday['total']}, yesterday: {$rYesterday['total']}  ";
    if(!isset($_SESSION['r'])){
        $_SESSION['r'] = $rToday['total'];    
    }
    
}


//CDR
echo $sqlCheckToday = " SELECT sum(duration) as total FROM cdr WHERE start >date(now()) ";
$cToday = $conn->doSelectOne($sqlCheckToday);

$timeCheck = date("Y-m-d H:i:s",time()-24*3600);
echo $sqlCheckYesterday = " SELECT sum(duration) as total FROM cdr WHERE start>=date('$timeCheck') AND start<='$timeCheck' ";
$cYesterday = $conn->doSelectOne($sqlCheckYesterday);

if($cToday['total']*2<$cYesterday['total']){
    $errorMsg[] = "Have problem on cdr, today: {$cToday['total']}, yesterday: {$cYesterday['total']}  ";
}

//SMS GW
$sms = str_replace("\n","<br/>",file_get_contents("http://localhost:13000/status?username=thiendv&password=thiendv"));
if(substr_count($sms,"Box connections")&&substr_count($sms,"SMSC connections")&&substr_count($sms,"Status: running")){
    echo "SMSC is working";
}else{
    $errorMsg[]= "SMSC ERROR<br/>".$sms;
}

//Report at 08h and 17h daily
$subject='';
if(date("H",time())=="08"||date("H",time())=="17"){
    if(date("i",time())>=0 and date("i",time())<=15){
        if(!count($errorMsg)) $subject = "1956 UNITEL REPORTING ";
        $errorMsg[]="System is working fine."
                   ."<br/> Revenue: Today = {$rToday['total']}KIP, Yesterday = {$rYesterday['total']}KIP "
                   ."<br/> HDD: {$hdd[4]}, available {$hdd[3]} "
                   ."<br/> CDR: today = {$cToday['total']}s, yesterday = {$cYesterday['total']}s "
                   ."<br/> CPU: $totalCPU%"
                   ."<br/> RAM: $totalRamUse%";
    }
}

var_dump($errorMsg);
 $sendContent='';
if(count($errorMsg)){
    foreach($errorMsg as $msg){
        $sendContent .= "$msg <br/>";
    }
    $mail = new PHPMailer(true);                //New instance, with exceptions enabled
    $mail->IsSMTP();                            // tell the class to use SMTP
    $mail->SMTPAuth   = true;                   // enable SMTP authentication
    $mail->Port       = 465;                     // set the SMTP server port
    $mail->Host       = "smtp.gmail.com";       // SMTP server
    $mail->SMTPSecure = 'ssl'; 
    $mail->Username   = $_email_account;     // SMTP server username
    $mail->Password   = $_email_password;           // SMTP server password

    $mail->From       = $_email_account;
    $mail->FromName   = "1956 Unitel";
	
    foreach($_mail_list as $to){
        $mail->AddAddress($to);
    }
    if($subject) $mail->Subject  = $subject;
    else $mail->Subject  = "1956 UNITEL ERROR";
    $mail->MsgHTML($sendContent);
    $mail->IsHTML(true);
    $mail->Send();
}
?>
