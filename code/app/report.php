<?php
//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");

try{
    $conn = new sbMysqlPDO($server, $user, $password, $db);
}catch(Exception $e){
    file_put_contents($realPath."log/{$mod}.txt","\nError when connect to database server|".date("Y-m-d H:i:s",time()));
    sleep(5);
}
//for($i=1;$i<=18;$i++){
    $data = array();
    $reportDate = date("Y-m-d",  strtotime("Yesterday"));
    //$reportDate = "2013-09-02";
    //$reportDate = "2013-06-".$i;
    $sqlRegister = "select count(*) as total,register_type from member where date(register_date)='{$reportDate}' GROUP BY  register_type ";
    $registers = $conn->doSelect($sqlRegister);
    foreach($registers as $register){
        if($register['register_type']==2) $data['new_register_sms'] = $register['total'];
        else if($register['register_type']==4) $data['new_register_auto'] = $register['total'];
        else $data['new_register_ivr'] = $register['total'];
    }
    $data['new_register'] = $data['new_register_ivr']+$data['new_register_sms']+$data['new_register_auto'];

    $sqlCancel = "select count(*) as total,status from member where date(cancel_date)='{$reportDate}' GROUP BY  status ";
    $cancels = $conn->doSelect($sqlCancel);
    foreach($cancels as $cancel){
        if($cancel['status']==4) $data['cancel_by_system'] = $cancel['total'];
        else if($cancel['status']==3) $data['cancel_by_user'] = $cancel['total'];
    }


    $data['total_cancel'] = $data['cancel_by_user']+$data['cancel_by_system'];

    $data['real_development'] = $data['new_register']-$data['total_cancel'];

    //var_dump($data);

    $sqlSelectCurrentUserCancel = " 
                                        SELECT count(*) as total 
                                        FROM member
                                        WHERE date(cancel_date)<='{$reportDate}'
                                  ";
    $totalCancel = $conn->doSelectOne($sqlSelectCurrentUserCancel);

    $sqlSelectCurrentUserRegister = " 
                                        SELECT count(*) as total 
                                        FROM member
                                        WHERE date(register_date)<='{$reportDate}'
                                  ";
    $totalRegister = $conn->doSelectOne($sqlSelectCurrentUserRegister);

    $data['current_user'] = $totalRegister['total']-$totalCancel['total'];

    $sqlCharge = " SELECT sum(total_money) as money_total, count(*) as total_charge FROM charging_history WHERE date(start_charging_date)='{$reportDate}' ";
   // echo $sqlCharge;
    $charge = $conn->doSelectOne($sqlCharge);
    $data['charge_able'] =  $charge['total_charge'];
    $data['total_revenue'] = $charge['money_total'];
    
    $sqlInsert = "INSERT INTO `report` (
                        `new_register`, `new_register_sms`, `new_register_ivr`, 
                        `cancel_by_user`, `cancel_by_system`, `total_cancel`,
                        `real_development`, `current_user`, `charge_able`,
                        `total_revenue`, `report_date`,new_register_auto
                        ) 
                VALUES (
                        '{$data['new_register']}', '{$data['new_register_sms']}', '{$data['new_register_ivr']}',
                        '{$data['cancel_by_user']}', '{$data['cancel_by_system']}', '{$data['total_cancel']}',
                        '{$data['real_development']}', '{$data['current_user']}', '{$data['charge_able']}',
                        '{$data['total_revenue']}', '{$reportDate}','{$data['new_register_auto']}'
                       )
                 ";
    $conn->doUpdate($sqlInsert);
//}
?>
