<?php

function addMoney($msisdn,$money,$type=10){
    $returnValue = 0;
	$result = '';
    try{
        $client = new SoapClient('http://10.78.6.226/ZSmartService/AccountService.asmx?wsdl');
        $addmoney->MSISDN = $msisdn;
        $addmoney->AcctResID = $type;
        $addmoney->AddBalance = "-".$money."00";
        try{
            $result = $client->ModifyBal($addmoney);
        }catch (Exception $e){
            $returnValue = 201;
        }
		if($result){
			$returnValue = 0;
		}else{
			echo "update fail \n";
			$returnValue = 203;
		}
    }catch(Exception $e){
        $returnValue = 202;
    }
    return $returnValue;
}

function addPromotion($msisdn,$money){
    global $_vasgateway;
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vas="http://www.unitel.com.la/vasgateway/">
		   <soapenv:Header/>
		   <soapenv:Body>
			  <vas:Input>
				 <username>'.$_vasgateway['username'].'</username>
				 <password>'.$_vasgateway['password'].'</password>
				 <wscode>addbalance</wscode>
				 <!--1 or more repetitions:-->
				<param name="GWORDER" value="1"/>
				<param name="input" value="'.$msisdn.'|0|promotion|10|'.$money.'|7"/>
			  </vas:Input>
		   </soapenv:Body>
		</soapenv:Envelope>';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    $result = 0;
    if($err || !$responeContent)
    {
        $result = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));

        $responeContent = explode('|', $responeContent);
        $result = $responeContent[0];
    }
    return $result;
}

function checkSubtype($msisdn){
    global $_vasgateway;
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vas="http://www.unitel.com.la/vasgateway/">
		   <soapenv:Header/>
		   <soapenv:Body>
			  <vas:Input>
				 <username>'.$_vasgateway['username'].'</username>
				 <password>'.$_vasgateway['password'].'</password>
				 <wscode>viewinfo</wscode>
				 <!--1 or more repetitions:-->
				<param name="GWORDER" value="1"/>
				<param name="input" value="'.$msisdn.'"/>
			  </vas:Input>
		   </soapenv:Body>
		</soapenv:Envelope>';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    $result = 0;
    if($err || !$responeContent)
    {
        $result = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));

        $responeContent = explode('|', $responeContent);
        $result = $responeContent[0];
    }
    return $result;
}

?>
