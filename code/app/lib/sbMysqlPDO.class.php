<?php

class sbMysqlPDO{
   
   private $con,$error,$traceError=1;
   
   function __construct($hostname,$username,$password,$dbname){
       $this->error = array();
       try{
           $this->con = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$password");
           $this->con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
       }catch(PDOException $e){
           $this->error[] = $e->getMessage();
           if($this->$traceError) echo $e->getMessage();
       }
   }
   
   public function setErrorReporting($status){
	$this->traceError = $status;	
   }

   public function doSelect($sql){
       try{
           $stmt = $this->con->prepare($sql);
           $stmt->execute();
           return $stmt->fetchAll();
       }catch(PDOException $e){
          $this->error[] = $e->getMessage();
          if($this->traceError) echo $e->getMessage();
          return false;
       }
   }

   public function doSelectOne($sql){
       try{
           $stmt = $this->con->prepare($sql);
           $stmt->execute();
           $data = $stmt->fetch(PDO::FETCH_ASSOC);
           if($data) return $data;
       }catch(PDOException $e){
           $this->error[] = $e->getMessage();
           if($this->traceError) echo $e->getMessage();
           return false;
       }
   }
   
   public function count($sql){
        try {
            $stmt = $this->con->prepare($sql);
            $stmt->execute();
            return $stmt->fetchColumn();
        } catch (PDOException $e) {
            $this->error[] = $e->getMessage();
            if($this->traceError) echo $e->getMessage();
            return false;
        }
    }

   public function getLastInsertId(){
       return $this->con->lastInsertId();
   }

   public function doUpdate($sql){
       try{
           $this->con->exec($sql);
           if($this->getLastInsertId()) return $this->getLastInsertId();
           else return true;
       }catch(PDOException $e){
           $this->error[] = $e->getMessage();
           if($this->traceError) echo $e->getMessage();
           return false;
       }
   }
   
   public function add($data,$table){
        try{
            if(is_array($data)){
                    $sqlInsert = "INSERT INTO {$table}";
                    $sqlField = "(";
                    $sqlValue = "VALUES(";
                    $i = 0;
                    foreach($data as $key=>$value){
                                    $value = str_replace("'","\'",$value);
                                    if($i){
                                                    $sqlField .= ",".$key;
                                                    $sqlValue .= ",'".$value."'";
                                    }else{
                                                    $sqlField .= $key;
                                                    $sqlValue .= "'".$value."'";
                                                    $i++;
                                    }
                    }
                    $sqlField .= ")";
                    $sqlValue .= ")";
                    $sqlInsert .= $sqlField.$sqlValue;
                    //echo $sqlInsert;
                    //exit();
                    $this->doUpdate($sqlInsert);
                    return $this->getLastInsertId();
            }
        }catch(Exception $e){
            $this->error[] = $e->getMessage();
            if($this->traceError) echo $e->getMessage();
            return false;
        }
    }

    
    public function update($data,$table,$conndition){
        try{
            if(is_array($data)){
                    $i=0;
                    $sqlField = "";
                    foreach($data as $key=>$value){
                            $value = str_replace("'","\'",$value);
                            if($i){
                                            $sqlField .= ",".$key."='".$value."'";
                            }else{
                                            $sqlField .= $key."='".$value."'";
                                            $i++;
                            }
                    }

                    $sql = "UPDATE {$table} set {$sqlField} WHERE {$conndition} ";
                    //echo $sql;
                    return $this->doUpdate($sql);
            }else{
                    return false;
            }
        }catch(Exception $e){
            $this->error[] = $e->getMessage();
            if($this->traceError) echo $e->getMessage();
            return false;
        }
    }

    public function delete($table,$condition){
        try{
            $sql = "DELETE FROM {$table} WHERE {$condition} ";
            return $this->doUpdate($sql);
        }catch(Exception $e){
            $this->error[] = $e->getMessage();
            if($this->traceError) echo $e->getMessage();
            return false;
	}
    }
    
    public function getConnection(){
       return $this->con;
   }
   
   public function getError(){
       return $this->error;
   }
   
   public function closeConnection(){
       $this->con = NULL;
   }
   
}

?>
